package mr.spatialindex;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;

public class AWSLogger {
	BufferedWriter bw = null;
	
	public BufferedWriter getWriter() {
		return bw;
	}

	public AWSLogger(String logfilename, Configuration conf) throws IOException {
		bw = new BufferedWriter(new OutputStreamWriter(SpatialIndex
				.getS3FileSystem(conf).create(new Path(logfilename))));
	}

	public void info(String info) throws IOException {
		bw.write(String.format("[I][%f][AWS]%s\n", System.nanoTime() / 1000.0, info));
		bw.flush();
		System.out.println(String.format("[I][%f][AWS]%s\n", System.nanoTime() / 1000.0, info));
	}

	public void debug(String debug) throws IOException {
		bw.write(String.format("[D][%f][AWS]%s\n", System.nanoTime() / 1000.0, debug));
		System.out.println(String.format("[D][%f][AWS]%s\n", System.nanoTime() / 1000.0, debug));
		bw.flush();
	}

	public void close() throws IOException {
		bw.close();
	}

}
