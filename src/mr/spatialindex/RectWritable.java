package mr.spatialindex;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;

public class RectWritable implements WritableComparable<RectWritable> {
	public Float[] lowerBounds;
	public Float[] upperBounds;

	public static int dimension = 2;

	public RectWritable() {
		lowerBounds = new Float[dimension];
		upperBounds = new Float[dimension];
	}

	@Override
	public void readFields(DataInput arg0) throws IOException {
		for (int i = 0; i < dimension; i++) {
			lowerBounds[i] = arg0.readFloat();
			upperBounds[i] = arg0.readFloat();
		}

	}

	@Override
	public void write(DataOutput arg0) throws IOException {
		// TODO Auto-generated method stub
		for (int i = 0; i < dimension; i++) {
			arg0.writeFloat(lowerBounds[i]);
			arg0.writeFloat(upperBounds[i]);
		}
	}

	@Override
	public int compareTo(RectWritable arg0) {
		// TODO Auto-generated method stub
		for (int i = 0; i < dimension; i++) {
			int r = lowerBounds[i].compareTo(arg0.lowerBounds[i]);
			if (r < 0)
				return -1;
			else if (r > 0)
				return 1;
		}
		return 0;
	}

}
