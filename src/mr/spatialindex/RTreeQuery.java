package mr.spatialindex;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import mr.spatialindex.RTreeMapReduce.RTMapper;
import mr.spatialindex.RTreeMapReduce.RTReducer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import spatialindex.rtree.Box;
import spatialindex.rtree.FileSystemSelector;
import spatialindex.rtree.LocalFSManager;
import spatialindex.rtree.QueryResult;
import spatialindex.rtree.RTree;

public class RTreeQuery {
	public static class RTQMapper extends
			Mapper<Object, Text, LongWritable, Text> {

		private int dimension;
		private HashMap<Box, Long> MBRMapper;

		@Override
		public void setup(Context context) throws IOException,
				InterruptedException {
			Configuration conf = context.getConfiguration();
			FileSystem fs = SpatialIndex.getS3FileSystem(conf);
			BufferedReader br = new BufferedReader(new InputStreamReader(
					fs.open(new Path(conf.get("conf_path")))));

			String line = null;
			HashMap<String, String> config = new HashMap<>();
			while ((line = br.readLine()) != null) {
				String[] kv = line.split(":");
				config.put(kv[0].trim(), kv[1].trim());
			}

			br.close();

			dimension = Integer.valueOf(config.get("dimension"));
			conf.setInt("dimension", dimension);
			MBRMapper = new HashMap<>();

			FileStatus[] files = fs
					.listStatus(new Path(conf.get("rtree_path")));
			for (FileStatus file : files) {
				if (file.isDirectory()
						&& file.getPath().getName().indexOf("ridx-") == 0) {
					String v = file.getPath().getName()
							.substring("ridx-".length());
					long groupId = Long.valueOf(v);
					Box mbr = new Box(dimension);
					String levelFilename = null;
					// find level file
					FileStatus[] idxFiles = fs.listStatus(file.getPath());
					for (FileStatus idxFile : idxFiles) {
						if (idxFile.getPath().getName()
								.indexOf("rdata-tree-level-") == 0) {
							levelFilename = idxFile.getPath().toString();
							break;
						}
					}
					System.out.printf("find level file %s\n", levelFilename);
					// after find it, read mbr from it
					FSDataInputStream fsdis = fs.open(new Path(levelFilename));
					for (int i = 0; i < dimension; i++) {
						mbr.lowerBounds[i] = fsdis.readFloat();
						mbr.upperBounds[i] = fsdis.readFloat();
					}
					fsdis.close();

					System.out.printf("read level file %s finished\n",
							levelFilename);

					MBRMapper.put(mbr, groupId);
				}
			}

		}

		private ArrayList<Long> getGroupId(String data) throws Exception {
			Box queryArea = new Box(dimension);
			String[] fields = data.split(",");
			ArrayList<Long> gids = new ArrayList<>();

			for (int i = 0; i < dimension; i++) {
				queryArea.lowerBounds[i] = Float.valueOf(fields[1 + i * 2]);
				queryArea.upperBounds[i] = Float.valueOf(fields[1 + i * 2 + 1]);
			}

			for (Map.Entry<Box, Long> kvp : MBRMapper.entrySet()) {
				if (kvp.getKey().intersect(queryArea)) {
					gids.add(kvp.getValue());
				}
			}

			return gids;
		}

		public void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
			ArrayList<Long> gids = null;
			try {
				gids = getGroupId(value.toString());
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (gids != null && gids.size() > 0) {
				for (Long gid : gids) {
					context.write(new LongWritable(gid), value);
				}
			} else {
				System.out.printf("find no rect\n");
			}
		}
	}

	public static class RTQReducer extends
			Reducer<LongWritable, Text, LongWritable, Text> {

		public String readDataToLocal(Configuration conf, long gid,
				AWSLogger awsLogger) throws IOException {
			String S3RTreeBaseDir = conf.get("rtree_path") + "/ridx-" + gid;
			String localDirBase = "/home/hadoop/ridx";
			String localRTreeBaseDir = localDirBase + "/ridx-" + gid;
			FileSystem fs = SpatialIndex.getS3FileSystem(conf);
			FileStatus[] idxFiles = fs.listStatus(new Path(S3RTreeBaseDir));

			// prepare for the input data
			LocalFSManager tfs = new LocalFSManager();
			if (!tfs.mkdir(localDirBase)) {
				awsLogger.info(localDirBase + " dir cannot be created");
				return null;
			} else {
				awsLogger.info(localDirBase + " dir is created");
			}

			if (!tfs.mkdir(localRTreeBaseDir)) {
				awsLogger.info(localRTreeBaseDir + " dir cannot be created");
				return null;
			} else {
				awsLogger.info(localRTreeBaseDir + " dir is created");
			}
			// read index from s3 to local
			for (FileStatus idxFile : idxFiles) {
				String src = idxFile.getPath().toString();
				String dest = localRTreeBaseDir + "/"
						+ idxFile.getPath().getName();
				awsLogger.info("copy file " + src + " to " + dest);

				DataOutputStream dos = new DataOutputStream(
						new FileOutputStream(dest));
				FSDataInputStream tfsdis = fs.open(idxFile.getPath());
				int bufferSize = 1024 * 1024;
				byte[] data = new byte[bufferSize];
				while (true) {
					int offset = 0;
					while (offset < bufferSize) {
						int readLen = tfsdis.read(data, offset, bufferSize
								- offset);
						if (readLen > 0)
							offset += readLen;
						else
							break;
					}
					if (offset > 0)
						dos.write(data, 0, offset);
					else
						break;
				}
				dos.close();
				tfsdis.close();
			}
			
			return localRTreeBaseDir + "/rdata";
		}

		public void reduce(LongWritable key, Iterable<Text> values,
				Context context) throws IOException, InterruptedException {
			Configuration conf = context.getConfiguration();
			int dimension = conf.getInt("dimension", 2);
			RTree rTree = new RTree(dimension);
			AWSLogger awsLogger = new AWSLogger(conf.get("log_file"), conf);
			String treeName = readDataToLocal(conf, key.get(), awsLogger);			
			
			FileSystemSelector.config(FileSystemSelector.LOCAL_FILESYSTEM_TYPE);
			rTree.construct(treeName);			
			
			for (Text value : values) {
				String[] fields = value.toString().split(",");
				long queryID = Long.valueOf(fields[0]);
				String results = "";
				Box queryArea = new Box(dimension);
				
				for (int i = 0; i < dimension; i++) {
					queryArea.lowerBounds[i] = Float.valueOf(fields[i * 2 + 1]);
					queryArea.upperBounds[i] = Float.valueOf(fields[i * 2 + 2]);
				}
				
				QueryResult qr = rTree.query(queryArea);
				for (long id : qr.resultIDs) {
					results += "" + id + ",";
				}
				
				results = results.substring(0, results.length() - 2);
				context.write(new LongWritable(queryID), new Text(results));
			}

		}
	}

	public static void work(String confFilename, String spFilename,
			String inputFilename, String outputFileFolder) throws Exception {
		Configuration conf = new Configuration();

		conf.setBoolean("fs.hdfs.impl.disable.cache", true);
		conf.set("conf_path", confFilename);
		conf.set("sp_res", spFilename);
		conf.set("rtree_path", outputFileFolder);
		conf.set("tmp_rtree_path", "");
		conf.set("log_file", "/log");

		Job job = Job.getInstance(conf, "Local RTree Constrcution");
		job.setJarByClass(RTreeMapReduce.class);
		job.setMapperClass(RTMapper.class);
		job.setReducerClass(RTReducer.class);

		job.setOutputKeyClass(LongWritable.class);
		job.setOutputValueClass(Text.class);

		job.setMapOutputKeyClass(LongWritable.class);
		job.setMapOutputValueClass(Text.class);

		FileInputFormat.addInputPath(job, new Path(inputFilename));
		FileOutputFormat.setOutputPath(job, new Path(outputFileFolder));
		job.waitForCompletion(true);
	}
}
