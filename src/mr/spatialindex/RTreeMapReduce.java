package mr.spatialindex;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.net.InetAddress;
import java.nio.ByteBuffer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import spatialindex.rtree.FileSystemSelector;
import spatialindex.rtree.HDFSManager;
import spatialindex.rtree.LocalFSManager;
import spatialindex.rtree.RTree;
import spatialindex.rtree.RTreeMemory;
import spatialindex.rtree.SystemLogger;
import spatialindex.sfcf.SFCFSceneMapper;

public class RTreeMapReduce {
	private static final int MEMORY_RTREE = 0;
	private static final int HDFS_RTREE = 1;
	private static final int LOCAL_RTREE = 2;

	public static class RTMapper extends
			Mapper<Object, Text, LongWritable, Text> {

		private ArrayList<Long> splitPoints;
		private int dimension;
		private SFCFSceneMapper sfcf = null;

		@Override
		public void setup(Context context) throws IOException,
				InterruptedException {
			Configuration conf = context.getConfiguration();
			FileSystem fs = SpatialIndex.getS3FileSystem(conf);

			splitPoints = new ArrayList<>();
			splitPoints.add(0L);
			// get split points
			BufferedReader br = new BufferedReader(new InputStreamReader(
					fs.open(new Path(conf.get("sp_res")))));
			String line = null;
			while ((line = br.readLine()) != null) {
				String[] fields = line.split("\t");
				splitPoints.add(Long.valueOf(fields[1]));
			}
			br.close();
			splitPoints.add(Long.MAX_VALUE);
			// get configurations
			br = new BufferedReader(new InputStreamReader(fs.open(new Path(conf
					.get("conf_path")))));

			line = null;
			HashMap<String, String> config = new HashMap<>();
			while ((line = br.readLine()) != null) {
				String[] kv = line.split(":");
				config.put(kv[0].trim(), kv[1].trim());
			}

			br.close();
			// initialize sfcf function
			String[] lbs = config.get("lowerBounds").split(",");
			String[] ubs = config.get("upperBounds").split(",");

			dimension = Integer.valueOf(config.get("dimension"));
			sfcf = new SFCFSceneMapper(dimension);
			conf.setInt("dimension", dimension);

			double[] dlbs = new double[dimension];
			double[] dubs = new double[dimension];
			int[] ncs = new int[dimension];
			for (int i = 0; i < dimension; i++) {
				dlbs[i] = Double.valueOf(lbs[i]);
				dubs[i] = Double.valueOf(ubs[i]);
				ncs[i] = Integer.valueOf(config.get("numCells"));
			}

			sfcf.setParams(dlbs, dubs, ncs);
		}

		private long computeSFCFValue(String data) throws Exception {
			double[] centers = new double[dimension];
			String[] fields = data.split(",");

			for (int i = 0; i < dimension; i++) {

				centers[i] = Double.valueOf(fields[1 + i * 2])
						+ Double.valueOf(fields[1 + i * 2 + 1]);

				centers[i] *= 0.5;
			}

			return sfcf.getZOrderValue(centers);
		}

		private long getPartitionNum(String data) {
			long sfcfv;
			try {
				sfcfv = computeSFCFValue(data);
			} catch (Exception e) {
				e.printStackTrace();
				return -1;
			}

			for (int i = 0; i < splitPoints.size() - 1; i++) {
				if (sfcfv >= splitPoints.get(i)
						&& sfcfv < splitPoints.get(i + 1))
					return i;
			}

			return -1;
		}

		public void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
			long pidx = getPartitionNum(value.toString());
			if (pidx >= 0) {
				context.write(new LongWritable(pidx), value);
			} else {
				System.out.printf("\033[33;1mData Error in \'%s\'\033[0m\n",
						value.toString());
			}
		}
	}

	public static class RTReducer extends
			Reducer<LongWritable, Text, LongWritable, Text> {

		private void storeIndex2S3(String rtreeBaseDir, String rtreeS3BaseDir,
				long groupId, AWSLogger awsLogger, FileSystem tfs,
				FileSystem pfs) throws IOException, FileNotFoundException {
			FileStatus[] files = tfs.listStatus(new Path(rtreeBaseDir));
			awsLogger.info("rtree construction successfully ends. gid:"
					+ groupId);

			for (FileStatus file : files) {
				String src = file.getPath().getName();
				String dest = rtreeS3BaseDir + "/" + src;
				awsLogger.info("copy file " + src + " to " + dest);

				FSDataOutputStream pfsdos = pfs.create(new Path(dest));
				FSDataInputStream tfsdis = tfs.open(file.getPath());
				int bufferSize = 1024 * 1024;
				byte[] data = new byte[bufferSize];
				while (true) {
					int offset = 0;
					while (offset < bufferSize) {
						int readLen = tfsdis.read(data, offset, bufferSize
								- offset);
						if (readLen > 0)
							offset += readLen;
						else
							break;
					}
					if (offset > 0)
						pfsdos.write(data, 0, offset);
					else
						break;
				}
				pfsdos.close();
				tfsdis.close();
			}

			awsLogger.info("transfer data finished");

		}

		private void hdfsRTreeConstruction(Iterable<Text> values, long groupId,
				Configuration conf, AWSLogger awsLogger) throws IOException {

			FileSystem tfs = SpatialIndex.getHDFSFileSystem(conf);
			FileSystem pfs = SpatialIndex.getS3FileSystem(conf);
			int dim = conf.getInt("dimension", 2);
			String rtreeBaseDir = "/" + "ridx-" + String.valueOf(groupId);
			String rtreeS3BaseDir = conf.get("rtree_path") + "/ridx-"
					+ String.valueOf(groupId);

			awsLogger.info("rtree base path is " + rtreeBaseDir);
			awsLogger.info("rtree S3 base path is " + rtreeS3BaseDir);
			awsLogger.info("begin external rtree construction. gid:" + groupId);
			awsLogger.info("IP Addr: "
					+ InetAddress.getLocalHost().getHostAddress());
			// prepare for the input data
			// hdfs working directory
			if (!tfs.mkdirs(new Path(rtreeBaseDir))) {
				awsLogger.info("create folder failed: " + rtreeBaseDir);
				return;
			} else {
				awsLogger.info("create folder succeed: " + rtreeBaseDir);
			}
			// s3 results directory
			if (!pfs.mkdirs(new Path(rtreeBaseDir))) {
				awsLogger.info("create folder failed: " + rtreeBaseDir);
				return;
			} else {
				awsLogger.info("create folder succeed: " + rtreeBaseDir);
			}
			// write them as binary data file
			FSDataOutputStream fsdos = tfs.create(new Path(rtreeBaseDir
					+ "/data-0"));
			FSDataOutputStream fsdos2 = tfs.create(new Path(rtreeBaseDir
					+ "/map"));
			long numRecords = 0;
			for (Text line : values) {
				String[] fields = line.toString().split(",");
				// write local data id
				fsdos.writeLong(numRecords);
				// write box boundary
				for (int i = 0; i < dim; i++) {
					fsdos.writeFloat(Float.valueOf(fields[1 + i * 2]));
					fsdos.writeFloat(Float.valueOf(fields[1 + i * 2 + 1]));
				}
				// write mapping relationship pair (new_id, old_id)
				fsdos2.writeLong(numRecords);
				fsdos2.writeLong(Long.valueOf(fields[0]));
				numRecords++;
			}
			fsdos.close();
			fsdos2.close();
			// check file size
			FileStatus files = tfs.getFileStatus(new Path(rtreeBaseDir
					+ "/data-0"));

			long nr = files.getLen() / (8 * (dim + 1));
			awsLogger.info("original dataset successfully write back: "
					+ rtreeBaseDir + "/data-0");
			awsLogger.info("there are totally " + numRecords + "; " + nr
					+ " records");
			awsLogger.info("rtree construction started. groupID:" + groupId);

			// ************************************************
			// construct r tree
			SystemLogger.setWriter(awsLogger.getWriter());
			FileSystemSelector.config(FileSystemSelector.HDFS_FILESYSTEM_TYPE);
			HDFSManager.init(tfs);
			RTree rTree = new RTree(dim);
			rTree.construct(rtreeBaseDir + "/data", nr);
			// check errors
			if (rTree.getErrors().size() > 0) {
				awsLogger.info("error happens");
				for (Exception e : rTree.getErrors()) {
					awsLogger.info(e.getMessage());
					StringWriter sw = new StringWriter();
					e.printStackTrace(new PrintWriter(sw));
					awsLogger.info(sw.toString());
				}
			} else {
				// store index
				storeIndex2S3(rtreeBaseDir, rtreeS3BaseDir, groupId, awsLogger,
						tfs, pfs);
			}
		}

		private void localDiskRTreeConstruction(Iterable<Text> values,
				long groupId, Configuration conf, AWSLogger awsLogger)
				throws IOException {
			int dim = conf.getInt("dimension", 2);
			String localDirBase = "/home/hadoop/ridx";
			String rtreeBaseDir = "/" + "ridx-" + String.valueOf(groupId);
			String rtreeS3BaseDir = conf.get("rtree_path") + "/ridx-"
					+ String.valueOf(groupId);

			awsLogger.info("rtree base path is " + rtreeBaseDir);
			awsLogger.info("rtree S3 base path is " + rtreeS3BaseDir);
			awsLogger.info("begin external rtree construction. gid:" + groupId);
			awsLogger.info("IP Addr: "
					+ InetAddress.getLocalHost().getHostAddress());
			// prepare for the input data
			LocalFSManager tfs = new LocalFSManager();
			if (!tfs.mkdir(localDirBase)) {
				awsLogger.info(localDirBase + " dir cannot be created");
				return;
			} else {
				awsLogger.info(localDirBase + " dir is created");
			}

			if (!tfs.mkdir(localDirBase + rtreeBaseDir)) {
				awsLogger.info(localDirBase + rtreeBaseDir
						+ " dir cannot be created");
				return;
			} else {
				awsLogger.info(localDirBase + rtreeBaseDir + " dir is created");
			}
			// write them as binary data file
			DataOutputStream fsdosd = new DataOutputStream(
					new FileOutputStream(localDirBase + rtreeBaseDir
							+ "/rdata-0"));
			DataOutputStream fsdosm = new DataOutputStream(
					new FileOutputStream(localDirBase + rtreeBaseDir + "/rmap"));
			long numRecords = 0;
			for (Text line : values) {
				String[] fields = line.toString().split(",");
				// write local data id
				fsdosd.writeLong(numRecords);
				// write box boundary
				for (int i = 0; i < dim; i++) {
					fsdosd.writeFloat(Float.valueOf(fields[1 + i * 2]));
					fsdosd.writeFloat(Float.valueOf(fields[1 + i * 2 + 1]));
				}
				// write mapping relationship pair (new_id, old_id)
				fsdosm.writeLong(numRecords);
				fsdosm.writeLong(Long.valueOf(fields[0]));
				numRecords++;
			}
			fsdosd.close();
			fsdosm.close();
			// constructing rtree
			awsLogger.info("original dataset successfully write back: "
					+ localDirBase + rtreeBaseDir + "/rdata-0");
			awsLogger.info("there are totally " + numRecords + " records");
			awsLogger.info("rtree construction started. groupID:" + groupId);
			SystemLogger.setWriter(awsLogger.getWriter());
			FileSystemSelector.config(FileSystemSelector.LOCAL_FILESYSTEM_TYPE);
			RTree rTree = new RTree(dim);
			rTree.construct(localDirBase + rtreeBaseDir + "/rdata", numRecords);
			// check errors
			if (rTree.getErrors().size() > 0) {
				awsLogger.info("error happens");
				for (Exception e : rTree.getErrors()) {
					awsLogger.info(e.getMessage());
					StringWriter sw = new StringWriter();
					e.printStackTrace(new PrintWriter(sw));
					awsLogger.info(sw.toString());
				}
				int tryTime = 2;
				String previousDataPath = localDirBase + rtreeBaseDir
						+ "/rdata-0";
				String previousDir = localDirBase + rtreeBaseDir;
				while (tryTime > 0) {
					awsLogger.info("**************************************************");
					awsLogger.info("**************************************************");
					awsLogger.info("try to construct rtree once more");
					rTree.reset();
					// make new work directory
					if (!tfs.mkdir(localDirBase + rtreeBaseDir + (2 - tryTime))) {
						awsLogger.info(localDirBase + rtreeBaseDir
								+ (2 - tryTime) + " dir cannot be created");
						return;
					}
					// move file to new dir
					if (!tfs.rename(previousDataPath, localDirBase
							+ rtreeBaseDir + (2 - tryTime) + "/rdata-0")) {
						awsLogger.info("move file failed");
						return;
					}
					tfs.delete(previousDir);
					previousDir = localDirBase + rtreeBaseDir + (2 - tryTime);
					previousDataPath = previousDir + "/rdata-0";

					rTree.construct(previousDir + "/rdata", numRecords);
					if (rTree.getErrors().size() == 0)
						break;
					tryTime--;
				}

				if (tryTime == 0)
					return;
				else {
					rtreeBaseDir = previousDir.substring(localDirBase.length());
				}
			}
			FileSystem pfs = SpatialIndex.getS3FileSystem(conf);
			awsLogger.info("successfully created rtree on local disk");
			// s3 results directory
			if (!pfs.mkdirs(new Path(rtreeS3BaseDir))) {
				awsLogger.info("create folder failed: " + rtreeS3BaseDir);
				return;
			} else {
				awsLogger.info("create folder succeed: " + rtreeS3BaseDir);
			}
			// transfer file from local to s3
			File idxDir = new File(localDirBase + rtreeBaseDir);
			String[] files = idxDir.list();
			for (String file : files) {
				String filename = localDirBase + rtreeBaseDir + "/" + file;
				File f = new File(filename);
				if (f.isDirectory() == false) {
					String destFilename = rtreeS3BaseDir + "/" + file;
					awsLogger.info("copy file " + filename + " to "
							+ destFilename);

					FSDataOutputStream pfsdos = pfs.create(new Path(
							destFilename));
					DataInputStream dis = new DataInputStream(
							new FileInputStream(f));
					int bufferSize = 1024 * 1024;
					byte[] data = new byte[bufferSize];
					while (true) {
						int offset = 0;
						while (offset < bufferSize) {
							int readLen = dis.read(data, offset, bufferSize
									- offset);
							if (readLen > 0)
								offset += readLen;
							else
								break;
						}
						if (offset > 0)
							pfsdos.write(data, 0, offset);
						else
							break;
					}
					pfsdos.close();
					dis.close();
				}
			}
			awsLogger.info("file transfer ended");
			tfs.delete(localDirBase + rtreeBaseDir);
		}

		private void memoryRTreeConstruction(Iterable<Text> values,
				long groupId, Configuration conf) throws IOException {
			FileSystem fs = SpatialIndex.getS3FileSystem(conf);
			RTreeMemory rtree = new RTreeMemory();

			// build rtree
			rtree.construct4Hadoop(values, conf.getInt("dimension", 2));
			ByteBuffer bb = rtree.buffer();
			byte[] bbary = new byte[bb.remaining()];
			bb.get(bbary);

			FSDataOutputStream fsdos = fs.create(new Path(conf
					.get("rtree_path") + "/ridx-" + String.valueOf(groupId)));
			fsdos.write(bbary);
			fsdos.close();

		}

		public void reduce(LongWritable key, Iterable<Text> values,
				Context context) throws IOException, InterruptedException {
			Configuration conf = context.getConfiguration();
			AWSLogger awsLogger = new AWSLogger(conf.get("log_file") + "-"
					+ key.get(), conf);
			int treeType = conf.getInt("rtree_type", MEMORY_RTREE);
			if (treeType == MEMORY_RTREE)
				memoryRTreeConstruction(values, key.get(), conf);
			else if (treeType == LOCAL_RTREE)
				localDiskRTreeConstruction(values, key.get(), conf, awsLogger);
			else if (treeType == HDFS_RTREE)
				hdfsRTreeConstruction(values, key.get(), conf, awsLogger);

			for (Text t : values) {
				context.write(key, t);
			}

			awsLogger.close();
		}
	}

	public static void work(String confFilename, String spFilename,
			String inputFilename, String outputFileFolder) throws Exception {
		Configuration conf = new Configuration();

		conf.setBoolean("fs.hdfs.impl.disable.cache", true);
		conf.set("conf_path", confFilename);
		conf.set("sp_res", spFilename);
		conf.set("rtree_path", outputFileFolder);
		conf.set("tmp_rtree_path", "");
		conf.setInt("rtree_type", LOCAL_RTREE);
		conf.set("log_file", "/log");

		Job job = Job.getInstance(conf, "Local RTree Constrcution");
		job.setJarByClass(RTreeMapReduce.class);
		job.setMapperClass(RTMapper.class);
		job.setReducerClass(RTReducer.class);

		job.setOutputKeyClass(LongWritable.class);
		job.setOutputValueClass(Text.class);

		job.setMapOutputKeyClass(LongWritable.class);
		job.setMapOutputValueClass(Text.class);

		FileInputFormat.addInputPath(job, new Path(inputFilename));
		FileOutputFormat.setOutputPath(job, new Path(outputFileFolder));
		job.waitForCompletion(true);
	}
}
