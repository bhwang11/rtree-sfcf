/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spatialindex.rtree;

import java.util.Arrays;

/**
 *
 * @author PowerWBH
 */
public class Box extends SpatialObject {

	public float[] lowerBounds;
	public float[] upperBounds;
	public int dimension;
	public long id;

	public Box(int dim) {
		dimension = dim;
		lowerBounds = new float[dim];
		upperBounds = new float[dim];

	}

	public void set(float[] lb, float[] ub) {
		System.arraycopy(lb, 0, lowerBounds, 0, dimension);
		System.arraycopy(ub, 0, upperBounds, 0, dimension);
	}
	
	public void setMinMax() {
		for (int i = 0; i < dimension; i++) {
			lowerBounds[i] = Float.MAX_VALUE;
			upperBounds[i] = Float.MIN_VALUE;
		}
	}

	public Box copy() {
		Box b = new Box(dimension);
		b.set(lowerBounds, lowerBounds);
		return b;
	}

	private boolean intersect1D(float vMin, float vMax, int idx) {
		return upperBounds[idx] >= vMin && lowerBounds[idx] <= vMax;
	}

	/**
	 * if intersected with b
	 *
	 * @param b
	 * @return
	 */
	public boolean intersect(Box b) {
		if (dimension != b.dimension) {
			return false;
		}

		for (int i = 0; i < dimension; i++) {
			if (!intersect1D(b.lowerBounds[i], b.upperBounds[i], i)) {
				return false;
			}

		}

		return true;
	}

	public static boolean intersect(Box b1, Box b2) {
		return b1.intersect(b2);
	}

	/**
	 * quadratic of minimal distance
	 *
	 * @param pos
	 * @return
	 */
	public float distSq(float[] pos) {
		float result = 0;
		for (int i = 0; i < dimension; i++) {
			float minDist = Math.min(Math.abs(pos[i] - lowerBounds[i]), Math.abs(pos[i] - upperBounds[i]));
			result += minDist * minDist;
		}

		return result;
	}

	public static float distSq(Box b, float[] pos) {
		return b.distSq(pos);
	}

	/**
	 * minimal distance
	 *
	 * @param pos
	 * @return
	 */
	public float dist(float[] pos) {
		return ((Double) Math.sqrt(distSq(pos))).floatValue();
	}

	public static float dist(Box b, float[] pos) {
		return b.dist(pos);
	}

	/**
	 * the volume of current box
	 *
	 * @return
	 */
	public float volume() {
		float vol = 1;
		for (int i = 0; i < dimension; i++) {
			vol *= upperBounds[i] - lowerBounds[i];
		}

		return vol;
	}

	public static float volume(Box b) {
		return b.volume();
	}

	public float enlargement(Box b) {
		float vol = 1;
		for (int i = 0; i < dimension; i++) {
			vol *= Math.max(upperBounds[i], b.upperBounds[i])
					- Math.min(lowerBounds[i], b.lowerBounds[i]);
		}

		return vol - volume();
	}

	public static float enlargement(Box b1, Box b2) {
		return b1.enlargement(b2);
	}

	public void add(Box b) {
		for (int i = 0; i < dimension; i++) {
			upperBounds[i] = Math.max(upperBounds[i], b.upperBounds[i]);
			lowerBounds[i] = Math.min(lowerBounds[i], b.lowerBounds[i]);
		}
	}

	public void add(int[] pos) {
		for (int i = 0; i < dimension; i++) {
			upperBounds[i] = Math.max(upperBounds[i], pos[i]);
			lowerBounds[i] = Math.min(lowerBounds[i], pos[i]);
		}
	}

	public Box union(Box b) {
		Box br = copy();
		br.add(b);

		return br;
	}

	/**
	 * Determine whether this box is equal to a given object. Equality is
	 * determined by the bounds of the box.
	 *
	 * @param o The object to compare with this box
	 * @return 
	 */
	@Override
	public boolean equals(Object o) {

		if (!(o instanceof Box)) {
			return false;
		} else {
			Box b = (Box) o;
			for (int i = 0; i < dimension; i++) {
				if (Math.abs(upperBounds[i] - b.upperBounds[i]) > Float.MIN_NORMAL * 2.0f
						|| Math.abs(lowerBounds[i] - b.lowerBounds[i]) > Float.MIN_NORMAL * 2.0f) {
					return false;
				}
			}
			return true;
		}
	}

	@Override
	public int hashCode() {
		int hash = 3;
		hash = 89 * hash + Arrays.hashCode(this.lowerBounds);
		hash = 89 * hash + Arrays.hashCode(this.upperBounds);
		hash = 89 * hash + this.dimension;
		return hash;
	}

	/**
	 * Return a string representation of this box, in the form: (1.2, 3.4),
	 * (5.6, 7.8)
	 *
	 * @return String String representation of this rectangle.
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < dimension - 1; i++) {
			sb.append(String.format("(%f, %f), ", lowerBounds[i], upperBounds[i]));
		}

		sb.append(String.format("(%f, %f), ", lowerBounds[dimension - 1], upperBounds[dimension - 1]));

		return sb.toString();
	}
	
	public Point center() {
		float[] centers = new float[dimension];
		for (int i = 0; i < dimension; i++) {
			centers[i] = (lowerBounds[i] + upperBounds[i]) * 0.5f;
		}
		
		return new Point(centers);
	}
	
	public boolean contain(Point p) {
		for (int i = 0; i < dimension; i++) {
			if (p.vals[i] < lowerBounds[i] || p.vals[i] > upperBounds[i]) {
				return false;
			}
		}
		
		return true;
	}

	@Override
	public boolean intersect(SpatialObject so) {
		if (so instanceof Point) {
			Point p = (Point)so;
			return contain(p);
		} else if (so instanceof Box) {
			Box b = (Box)so;
			return intersect(b);
		}
		
		return false;		
	}
}
