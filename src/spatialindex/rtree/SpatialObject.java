package spatialindex.rtree;



public abstract class SpatialObject {
	public abstract boolean intersect(SpatialObject so);
}
