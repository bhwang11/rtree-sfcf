/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spatialindex.rtree;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import sun.misc.Cleaner;
import sun.nio.ch.DirectBuffer;

/**
 * 
 * @author PowerWBH
 */
public class Sorter {

	int numOfItem = 0;
	int blockSize = defaultBlockSize;

	public static final long itemSize = 8L + 4L;
	public static final int defaultBlockSize = 1 << 20;
	public static boolean bUseConcurrentSort = true;
	public static boolean bShrinkOrigFile = true;

	static final int numLocks = 4;
	Lock[] locks;

	class SortNode implements Comparable<SortNode> {

		public long id;
		public float key;

		@Override
		public int compareTo(SortNode o) {
			if (key < o.key) {
				return -1;
			} else if (key == o.key) {
				return 0;
			} else {
				return 1;
			}
		}
	}

	class MergeNode implements Comparable<MergeNode> {

		public int id;
		public SortNode key;

		@Override
		public int compareTo(MergeNode m) {
			return key.compareTo(m.key);
		}
	}

	class RecordNode {

		public long id;
		public Point data;
	}

	class SingleSorterThread implements Runnable {

		String filename;
		long startIdx;
		long numItem;
		boolean bWriteBack;
		boolean bConcurrent;
		SortNode[] points;
		int threadID;

		/**
		 * constructor for single thread sorter
		 * 
		 * @param file
		 *            file to be sorted
		 * @param si
		 *            start index, item size is decided by itemSize
		 * @param nitem
		 *            number of items
		 * @param bwb
		 *            whether data is write back to disk
		 * @param bc
		 *            whether this object is used concurrently
		 * @param id
		 *            allocate id
		 */
		public SingleSorterThread(String file, long si, long nitem,
				boolean bwb, boolean bc, int id) {
			points = new SortNode[(int) nitem];
			startIdx = si;
			numItem = nitem;
			filename = file;
			bWriteBack = bwb;
			threadID = id;
			bConcurrent = bc;
		}

		/**
		 * sort part of the file
		 */
		@Override
		public void run() {
			if (bConcurrent) {
				locks[threadID % numLocks].lock();
			}

			SystemLogger
					.debug(String
							.format("[thrd %d] sort start filename=\'%s\'; si=\'%d\'; ni=\'%d\'",
									Thread.currentThread().getId(), filename,
									startIdx, numItem));

			// SystemLogger.getLogger(Sorter.class).info();
			try {
				FileSystemManager fsReader = FileSystemSelector.getFileSystem();
				fsReader.mapInputFile(filename, startIdx * itemSize, numItem
						* itemSize);
				// FileChannel fc = new FileInputStream(filename).getChannel();
				// MappedByteBuffer mbb = fc.map(FileChannel.MapMode.READ_ONLY,
				// startIdx * itemSize, numItem * itemSize);

				for (int i = 0; i < numItem; i++) {
					points[i] = new SortNode();
					points[i].id = fsReader.getLong();// mbb.getLong();
					points[i].key = fsReader.getFloat();// mbb.getFloat();
				}

				// fc.close();
				// unmap(fc, mbb);
				fsReader.unmapInputFile();

				// sort file
				Arrays.sort(points);
				// write back to file
				if (!bWriteBack) {
					SystemLogger
							.debug(String
									.format("[thrd %d] sort end filename=\'%s\'; si=\'%d\'; ni=\'%d\'",
											Thread.currentThread().getId(),
											filename, startIdx, numItem));
					if (bConcurrent) {
						locks[threadID % numLocks].unlock();
					}

					return;
				}

				ByteBuffer bb = ByteBuffer.allocateDirect((int) numItem
						* (int) itemSize);
				for (int i = 0; i < numItem; i++) {
					bb.putLong(points[i].id);
					bb.putFloat(points[i].key);
				}

				// FileChannel fco = new FileOutputStream(filename + "-"
				// + threadID).getChannel();
				FileSystemManager fsWriter = FileSystemSelector.getFileSystem();
				fsWriter.mapOutputFile(filename + "-" + threadID);
				bb.flip();

				// fco.write(bb);
				// fco.close();
				fsWriter.write(bb);
				fsWriter.unmapOutputFile();
				clean(bb);

			} catch (FileNotFoundException ex) {
				SystemLogger.error(ex);
			} catch (IOException ex) {
				SystemLogger.error(ex);
			} catch (Exception ex) {
				SystemLogger.error(ex);
			}

			SystemLogger
					.debug(String
							.format("[thrd %d] sort end filename=\'%s\'; si=\'%d\'; ni=\'%d\'",
									Thread.currentThread().getId(), filename,
									startIdx, numItem));
			if (bConcurrent) {
				locks[threadID % numLocks].unlock();
			}

		}

	}

	public Sorter() {
	}

	/**
	 * unmap MappedByteBuffer object. It is used to recycle the memory.
	 * 
	 * @param fc
	 *            binded FileChannel object
	 * @param bb
	 *            MappedByteBuffer object
	 * @throws Exception
	 */
	public static void unmap(FileChannel fc, MappedByteBuffer bb)
			throws Exception {
		Class<?> fcClass = fc.getClass();
		java.lang.reflect.Method unmapMethod = fcClass.getDeclaredMethod(
				"unmap", new Class[] { java.nio.MappedByteBuffer.class });
		unmapMethod.setAccessible(true);
		unmapMethod.invoke(null, new Object[] { bb });
	}

	/**
	 * clean the direct type ByteBuffer. reuse memory, in case of OutOfMemory
	 * Exception
	 * 
	 * @param bb
	 */
	public static void clean(ByteBuffer bb) {
		if (bb == null) {
			return;
		}
		Cleaner cleaner = ((DirectBuffer) bb).cleaner();
		if (cleaner != null) {
			cleaner.clean();
		}
	}

	/**
	 * sort data in file. It can choose whether external sort is needed.
	 * 
	 * @param filename
	 *            data file name
	 * @param startIdx
	 *            start index
	 * @param numItems
	 *            number of items to be sorted
	 */
	public void sort(String filename, long startIdx, long numItems) {
		long numOfWay = 256;
		try {
			numOfWay = (long) Math.ceil((double) numItems / blockSize);
			if (numOfWay == 1) {
				SingleSorterThread sst = new SingleSorterThread(filename,
						startIdx, numItems, true, false, 0);
				sst.run();
				if (!FileSystemSelector.getFileSystem().rename(filename + "-0",
						filename + "-sort")) {
					throw new Exception("rename failed at the end of sort");
				}
			} else {
				blockSize = defaultBlockSize >> 1;
				numOfWay = (long) Math.ceil((double) numItems / blockSize);

				int iNumOfWay = (int) numOfWay;
				long[] pointer = new long[iNumOfWay];
				long[] sizes = new long[iNumOfWay];
				FileSystemManager[] fsReader = new FileSystemManager[iNumOfWay];
				// FileChannel[] fcs = new FileChannel[iNumOfWay];
				// MappedByteBuffer[] mbbs = new MappedByteBuffer[iNumOfWay];

				if (bUseConcurrentSort == true) {
					// sort blocks
					ExecutorService es = Executors.newFixedThreadPool(numLocks);
					SingleSorterThread[] ssts = new SingleSorterThread[iNumOfWay];

					locks = new ReentrantLock[numLocks];
					for (int i = 0; i < numLocks; i++) {
						locks[i] = new ReentrantLock();
					}

					for (int i = 0; i < numOfWay; i++) {
						locks[i % numLocks].lock();

						long si = startIdx + i * blockSize;
						long size = Math.min(startIdx + numItems - si,
								blockSize);

						ssts[i] = new SingleSorterThread(filename, si, size,
								true, true, i);
						pointer[i] = 0;
						sizes[i] = size;

						es.execute(ssts[i]);

						locks[i % numLocks].unlock();
					}

					es.shutdown();
					while (!es.awaitTermination(2, TimeUnit.SECONDS))
						;
				} else {
					for (int i = 0; i < numOfWay; i++) {
						long si = startIdx + i * blockSize;
						long size = Math.min(startIdx + numItems - si,
								blockSize);

						SingleSorterThread sst = new SingleSorterThread(
								filename, si, size, true, false, i);
						sst.run();

						pointer[i] = 0;
						sizes[i] = size;
					}
				}

				SystemLogger.debug("***********************************");
				SystemLogger.debug("***********************************");
				SystemLogger.debug("sort finished, merge begins");

				// merge blocks
				// **********************************************
				PriorityQueue<MergeNode> heap = new PriorityQueue<>(iNumOfWay);
				ByteBuffer bb = ByteBuffer.allocateDirect(blockSize
						* (int) itemSize);
				FileSystemSelector.getFileSystem().delete(filename + "-sort");
				FileSystemManager fsWriter = FileSystemSelector.getFileSystem();
				fsWriter.mapOutputFile(filename + "-sort");
				// FileChannel fc = new FileOutputStream(filename + "-sort")
				// .getChannel();

				int counter = 0;

				for (int i = 0; i < numOfWay; i++) {
					// fcs[i] = new FileInputStream(filename + "-" + i)
					// .getChannel();
					// mbbs[i] = fcs[i].map(FileChannel.MapMode.READ_ONLY, 0,
					// sizes[i] * itemSize);
					fsReader[i] = FileSystemSelector.getFileSystem();
					fsReader[i].mapInputFile(filename + "-" + i, 0, sizes[i]
							* itemSize);
				}
				// in case that the last file has no data inside
				if (fsReader[iNumOfWay - 1].size() == 0) {
					SystemLogger
							.warning("last file contain no data when sorting file "
									+ filename);
					fsReader[iNumOfWay - 1].unmapInputFile();
					numOfWay--;
					iNumOfWay--;

					// fcs[(int) numOfWay - 1].close();
					// unmap(fcs[(int) numOfWay - 1], mbbs[(int) numOfWay - 1]);
					// numOfWay--;
				}

				for (int i = 0; i < numOfWay; i++) {
					MergeNode mn = new MergeNode();
					mn.key = new SortNode();
					mn.key.id = fsReader[i].getLong();// mbbs[i].getLong();
					mn.key.key = fsReader[i].getFloat();// mbbs[i].getFloat();
					mn.id = i;

					heap.add(mn);
					pointer[i]++;
				}

				SystemLogger.debug("merge init finished, start merging");

				while (true) {
					boolean bFinished = true;
					for (int i = 0; i < numOfWay; i++) {
						if (pointer[i] < sizes[i]) {
							bFinished = false;
							break;
						}
					}

					if (bFinished) {
						break;
					}
					// extract one item
					MergeNode mn = heap.poll();

					bb.putLong(mn.key.id);
					bb.putFloat(mn.key.key);
					counter++;
					// add one item
					if (pointer[mn.id] < sizes[mn.id]) {
						MergeNode mn1 = new MergeNode();
						mn1.key = new SortNode();
						mn1.key.id = fsReader[mn.id].getLong();// mbbs[mn.id].getLong();
						mn1.key.key = fsReader[mn.id].getFloat();// mbbs[mn.id].getFloat();
						mn1.id = mn.id;

						heap.add(mn1);
						pointer[mn.id]++;
					}
					// if write back
					if (counter >= blockSize) {
						bb.position(0);
						// fc.write(bb);
						fsWriter.write(bb);
						counter = 0;
						bb.clear();
					}
				}

				for (int i = 0; i < numOfWay; i++) {
					fsReader[i].unmapInputFile();
					FileSystemSelector.getFileSystem().delete(
							filename + "-" + i);
					// if (fcs[i].isOpen()) {
					// fcs[i].close();
					// unmap(fcs[i], mbbs[i]);
					// delete(filename + "-" + i);
					// }
				}

				while (heap.size() > 0) {
					MergeNode mn = heap.poll();

					bb.putLong(mn.key.id);
					bb.putFloat(mn.key.key);
					counter++;

					// if write back
					if (counter >= blockSize) {
						bb.position(0);
						// fc.write(bb);
						fsWriter.write(bb);
						counter = 0;
						bb.clear();
					}
				}

				if (counter >= 0) {
					bb.flip();
					// fc.write(bb);
					fsWriter.write(bb);
					counter = 0;
				}

				clean(bb);
				// fc.close();
				fsWriter.unmapOutputFile();
			}
			SystemLogger.debug("merge successfully ended");
		} catch (Exception e) {
			SystemLogger.error(e);
		}

	}

	/**
	 * split file into numPartition partitions and write them back to separate
	 * files. Items in each newly split file are sorted by id
	 * 
	 * @param filename
	 *            file to be split
	 * @param numPartition
	 *            number of partitions
	 * @param num
	 *            number of items in file (not accurate, used for evaluating
	 *            which method is used)
	 * @return the number of items in each partition (not totally accurate. For
	 *         example, the last partition file always has less items)
	 */
	public long makePartition(String filename, int numPartition, long num) {
		long numPerPatition = (long) Math.ceil((double) num / numPartition);
		if (numPerPatition >= defaultBlockSize) {
			int i = 0;
			for (long si = 0; si < num; si += numPerPatition, i++) {
				try {
					// FileChannel fc = new
					// FileInputStream(filename).getChannel();
					// long size = Math.min(num - si, numPerPatition);
					// MappedByteBuffer mbbi = fc.map(
					// FileChannel.MapMode.READ_ONLY, si * itemSize, size
					// * itemSize);

					FileSystemManager fsReader = FileSystemSelector
							.getFileSystem();
					long size = Math.min(num - si, numPerPatition);
					fsReader.mapInputFile(filename, si * itemSize, size
							* itemSize);

					FileSystemManager fsWriter = FileSystemSelector
							.getFileSystem();
					fsWriter.mapOutputFile(filename + "-p-" + i);

					// FileChannel fcout = new FileOutputStream(filename + "-p-"
					// + i).getChannel();

					ByteBuffer bb = ByteBuffer
							.allocateDirect((int) (defaultBlockSize * itemSize));
					int counter = 0;

					for (int j = 0; j < size; j++) {
						long id = fsReader.getLong(); // mbbi.getLong();
						// float key = fsReader.getFloat(); // mbbi.getFloat();
						fsReader.getFloat();

						bb.putLong(id);
						bb.putFloat((float) id);
						counter++;

						if (counter >= defaultBlockSize) {
							bb.position(0);
							// fcout.write(bb);
							fsWriter.write(bb);
							counter = 0;
							bb.clear();
						}
					}

					if (counter >= 0) {
						bb.flip();
						// fcout.write(bb);
						fsWriter.write(bb);
					}

					// fc.close();
					// fcout.close();
					// unmap(fc, mbbi);
					fsReader.unmapInputFile();
					fsWriter.unmapOutputFile();
					clean(bb);

					sort(filename + "-p-" + i, 0, size);
					FileSystemSelector.getFileSystem().delete(
							filename + "-p-" + i);
					FileSystemSelector.getFileSystem().rename(
							filename + "-p-" + i + "-sort",
							filename + "-p-" + i);
				} catch (Exception e) {
					SystemLogger.error(e);
				}
			}
		} else {
			int i = 0;
			for (long si = 0; si < num; si += numPerPatition, i++) {
				try {
					// FileChannel fc = new
					// FileInputStream(filename).getChannel();
					// MappedByteBuffer mbb = fc.map(
					// FileChannel.MapMode.READ_ONLY, si * itemSize, size
					// * itemSize);

					long size = Math.min(num - si, numPerPatition);
					FileSystemManager fsReader = FileSystemSelector
							.getFileSystem();
					fsReader.mapInputFile(filename, si * itemSize, size
							* itemSize);

					SortNode[] points = new SortNode[(int) size];
					ByteBuffer bb = ByteBuffer.allocateDirect((int) size
							* (int) itemSize);

					for (int j = 0; j < size; j++) {
						points[j] = new SortNode();
						points[j].id = fsReader.getLong(); // mbb.getLong();
						points[j].key = fsReader.getFloat(); // mbb.getFloat();
						points[j].key = points[j].id;
					}

					// fc.close();
					// unmap(fc, mbb);
					fsReader.unmapInputFile();

					// sort file
					Arrays.sort(points);

					for (int j = 0; j < size; j++) {
						bb.putLong(points[j].id);
						bb.putFloat(points[j].key);
					}

					// FileChannel fcout = new FileOutputStream(filename + "-p-"
					// + i).getChannel();
					FileSystemManager fsWriter = FileSystemSelector
							.getFileSystem();
					fsWriter.mapOutputFile(filename + "-p-" + i);

					// bb.position(0);
					bb.flip();
					// fcout.write(bb);
					// fcout.close();
					fsWriter.write(bb);
					fsWriter.unmapOutputFile();
					clean(bb);
				} catch (Exception e) {
					SystemLogger.error(e);
				}
			}
		}

		return numPerPatition;
	}

	private RecordNode[] readRecordFromFile(String partitionFile,
			String origFile, int endDim, int recordSize) {
		try {
			// FileChannel fc = new FileInputStream(partitionFile).getChannel();
			// long size1 = fc.size() / itemSize, p1 = 0;
			// MappedByteBuffer mbb = fc.map(FileChannel.MapMode.READ_ONLY, 0,
			// fc.size());

			FileSystemManager fsReader = FileSystemSelector.getFileSystem();
			fsReader.mapInputFile(partitionFile, 0, defaultBlockSize * itemSize);

			long size1 = fsReader.size() / itemSize, p1 = 0;
			RecordNode[] points = new RecordNode[(int) size1];

			// FileChannel ofc = new FileInputStream(origFile).getChannel();
			// long size2 = ofc.size() / recordSize, p2 = 0;
			// MappedByteBuffer ombb = ofc.map(FileChannel.MapMode.READ_ONLY, 0,
			// ofc.size());
			FileSystemManager fsoReader = FileSystemSelector.getFileSystem();
			fsoReader.mapInputFile(origFile, 0, 1L << 50L);
			long size2 = fsoReader.size() / recordSize, p2 = 0;

			ByteBuffer bb = ByteBuffer
					.allocateDirect((int) (defaultBlockSize * recordSize));
			// FileChannel fcout = new FileOutputStream(origFile + "-less")
			// .getChannel();
			FileSystemManager fsWriter = null;
			if (bShrinkOrigFile) {
				fsWriter = FileSystemSelector.getFileSystem();
				fsWriter.mapOutputFile(origFile + "-less");
			}

			int counter = 0;

			long id1 = fsReader.getLong(); // mbb.getLong();
			fsReader.getFloat(); // mbb.getFloat();

			long id2 = fsoReader.getLong(); // ombb.getLong();
			Point point2 = new Point(endDim);

			for (int i = 0; i < endDim; i++) {
				point2.vals[i] = fsoReader.getFloat(); // ombb.getFloat();
			}

			while (p1 < size1 && p2 < size2) {
				if (id1 < id2) {
					p1++;
					if (p1 < size1) {
						id1 = fsReader.getLong(); // mbb.getLong();
						fsReader.getFloat(); // mbb.getFloat();
					}
				} else if (id1 > id2) {
					bb.putLong(id2);
					for (int i = 0; i < endDim; i++) {
						bb.putFloat(point2.vals[i]);
					}

					counter++;
					if (counter >= defaultBlockSize) {
						bb.position(0);
						// fcout.write(bb);
						if (bShrinkOrigFile)
							fsWriter.write(bb);

						bb.clear();
						counter = 0;
					}

					p2++;
					if (p2 < size2) {
						id2 = fsoReader.getLong(); // ombb.getLong();
						for (int i = 0; i < endDim; i++) {
							point2.vals[i] = fsoReader.getFloat(); // ombb.getFloat();
						}
					}
				} else {
					points[(int) p1] = new RecordNode();
					points[(int) p1].id = id1;
					points[(int) p1].data = new Point(point2);
					// increment p1
					p1++;
					if (p1 < size1) {
						id1 = fsReader.getLong(); // mbb.getLong();
						fsReader.getFloat(); // mbb.getFloat();
					}
					// increment p2
					p2++;
					if (p2 < size2) {
						id2 = fsoReader.getLong(); // ombb.getLong();
						for (int i = 0; i < endDim; i++) {
							point2.vals[i] = fsoReader.getFloat(); // ombb.getFloat();
						}
					}
				}
			}

			while (p2 < size2) {
				bb.putLong(id2);
				for (int i = 0; i < endDim; i++) {
					bb.putFloat(point2.vals[i]);
				}

				counter++;
				if (counter >= defaultBlockSize) {
					bb.position(0);
					// fcout.write(bb);

					if (bShrinkOrigFile)
						fsWriter.write(bb);
					bb.clear();
					counter = 0;
				}

				p2++;
				if (p2 < size2) {
					id2 = fsoReader.getLong(); // ombb.getLong();
					for (int i = 0; i < endDim; i++) {
						point2.vals[i] = fsoReader.getFloat(); // ombb.getFloat();
					}
				}
			}

			// fc.close();
			// unmap(fc, mbb);
			// ofc.close();
			// unmap(ofc, ombb);
			fsReader.unmapInputFile();
			fsoReader.unmapInputFile();

			if (counter > 0) {
				bb.flip();
				// fcout.write(bb);
				if (bShrinkOrigFile)
					fsWriter.write(bb);
			}

			// fcout.close();
			if (bShrinkOrigFile)
				fsWriter.unmapOutputFile();

			clean(bb);

			if (bShrinkOrigFile) {
				FileSystemSelector.getFileSystem().delete(origFile);
				FileSystemSelector.getFileSystem().rename(origFile + "-less",
						origFile);
			}

			return points;
		} catch (Exception e) {
			SystemLogger.error(e);
			return null;
		}
	}

	private void sortOneDim(RecordNode[] points, int dim, int startIdx,
			int endIdx) {
		final int d = dim;
		Arrays.sort(points, startIdx, endIdx, new Comparator<RecordNode>() {

			@Override
			public int compare(RecordNode o1, RecordNode o2) {
				if (o1.data.vals[d] < o2.data.vals[d]) {
					return -1;
				} else if (o1.data.vals[d] > o2.data.vals[d]) {
					return 1;
				} else {
					return 0;
				}
			}
		});
	}

	private void makePartitionOnMemory(RecordNode[] points, int startIdx,
			int endIdx, int startDim, int endDim, int numPartition,
			RTreeNode parent) {
		if (startDim < endDim) {
			int numPerPartition = (int) Math.ceil((double) (endIdx - startIdx)
					/ numPartition);
			numPartition = (int) Math.ceil((double) (endIdx - startIdx)
					/ numPerPartition);

			sortOneDim(points, startDim, startIdx, endIdx);
			parent.mp = parent.new MemoryPointer();
			parent.mp.children = new RTreeNode[numPartition];

			for (int idx = startIdx, cnt = 0; idx < endIdx; idx += numPerPartition, cnt++) {
				int si = idx;
				int ei = Math.min(numPerPartition + si, endIdx);

				parent.mp.children[cnt] = new RTreeNode();
				makePartitionOnMemory(points, si, ei, startDim + 1, endDim,
						numPartition, parent.mp.children[cnt]);
			}
		} else {
			parent.mp = null;
			parent.dp = parent.new DiskPointer();
			parent.dp.children = new long[endIdx - startIdx];
			for (int i = startIdx; i < endIdx; i++) {
				parent.dp.children[i - startIdx] = points[i].id;
			}

			// SystemLogger.info("finished one partition size is " + (endIdx -
			// startIdx));
			// try {
			// System.in.read();
			// } catch (IOException ex) {
			// Logger.getLogger(Sorter.class.getName()).log(Level.SEVERE, null,
			// ex);
			// }
		}
	}

	private void retriveAllPartitions(RTreeNode root,
			ArrayList<RTreeNode> partitions) {
		if (root.mp == null) {
			partitions.add(root);
		} else {
			for (int i = 0; i < root.mp.children.length; i++) {
				retriveAllPartitions(root.mp.children[i], partitions);
			}
		}
	}

	/**
	 * Split the rectangle center on memory. This function is used for small
	 * size data that can be split on memory. It starts splitting data from
	 * dimension startDim and ends at endDim. Each invoke will shrink the
	 * original data file size. The reason is that we need to read all needed
	 * data from file of all dimension. At least one pass is needed to
	 * accomplish this. Therefore, to make next time invoke faster, we remove
	 * matched item from the original file and thereby shrink them. Although it
	 * will cost extra write back procedure, it reduces the times of redundant
	 * comparison of the same item.
	 * 
	 * @param filename
	 *            the name of file that contain id set
	 * @param origFilename
	 *            original data file name
	 * @param startDim
	 *            dim start id
	 * @param endDim
	 *            dim end id
	 * @param recordSize
	 *            the size of each record
	 * @param numPartition
	 *            the number of the partition on each dimension
	 * @return all partitions
	 */
	public ArrayList<RTreeNode> makeRestPartition(String filename,
			String origFilename, int startDim, int endDim, int recordSize,
			int numPartition) {
		RecordNode[] points = readRecordFromFile(filename, origFilename,
				endDim, recordSize);
		RTreeNode rtn = new RTreeNode();
		ArrayList<RTreeNode> partitions = new ArrayList<>();

		// for (int i = 0; i < points.length; i++) {
		// System.out.printf("%d\t%f, %f\n", points[i].id,
		// points[i].data.vals[0], points[i].data.vals[1]);
		// }
		// try {
		// System.in.read();
		// } catch (IOException ex) {
		//
		// }
		makePartitionOnMemory(points, 0, points.length, startDim, endDim,
				numPartition, rtn);
		retriveAllPartitions(rtn, partitions);

		return partitions;
	}

	public ArrayList<RTreeNode> makePartition(ArrayList<RTreeNode> boxes,
			int endDim, int numPartition) {
		RecordNode[] points = new RecordNode[boxes.size()];
		RTreeNode rtn = new RTreeNode();
		ArrayList<RTreeNode> partitions = new ArrayList<>();

		for (int i = 0; i < boxes.size(); i++) {
			points[i] = new RecordNode();
			points[i].id = boxes.get(i).id;

			Box b = new Box(endDim);
			b.set(boxes.get(i).lowerBounds, boxes.get(i).upperBounds);
			points[i].data = b.center();
		}

		makePartitionOnMemory(points, 0, points.length, 0, endDim,
				numPartition, rtn);
		retriveAllPartitions(rtn, partitions);

		return partitions;
	}

	/**
	 * join two sorted file data based on id.
	 * 
	 * @param fn1
	 *            data file 1
	 * @param fn2
	 *            data file 2
	 * @param n1
	 *            number of items in file 1
	 * @param n2
	 *            number of items in file 2
	 * @return the number of result items
	 */
	public long join(String fn1, String fn2, String ofn, long n1, long n2) {
		if (n1 < defaultBlockSize) {
			try {
				SortNode[] points = new SortNode[(int) n1];
				ByteBuffer bb = ByteBuffer
						.allocateDirect((int) (n1 * itemSize));
				// FileChannel fc = new FileInputStream(fn1).getChannel();
				// n1 = Math.min(n1, fc.size() / itemSize);
				// fc.read(bb);
				// fc.close();

				FileSystemManager fsReader = FileSystemSelector.getFileSystem();
				n1 = fsReader.readAll(fn1, bb) / itemSize;

				bb.flip();
				for (int i = 0; i < n1; i++) {
					try {
						points[i] = new SortNode();
						points[i].id = bb.getLong();
						points[i].key = bb.getFloat();
					} catch (Exception e) {
						SystemLogger.error(e);
						SystemLogger.debug(fn1 + " " + fn2);
					}

				}
				clean(bb);

				// fc = new FileInputStream(fn2).getChannel();
				// MappedByteBuffer mbb = fc.map(FileChannel.MapMode.READ_ONLY,
				// 0,
				// n2 * itemSize);
				fsReader = FileSystemSelector.getFileSystem();
				fsReader.mapInputFile(fn2, 0, n2 * itemSize);
				bb = ByteBuffer
						.allocateDirect((int) (defaultBlockSize * itemSize));

				// FileChannel fcout = new FileOutputStream(fn1 + "-join")
				// .getChannel();
				FileSystemManager fsWritter = FileSystemSelector
						.getFileSystem();
				fsWritter.mapOutputFile(ofn);

				int pointer1 = 0;
				long curId = fsReader.getLong();// mbb.getLong();
				float curKey = fsReader.getFloat();// mbb.getFloat();
				long pointer2 = 0;
				int counter = 0;

				while (pointer1 < n1 && pointer2 < n2) {
					if (points[pointer1].id < curId) {
						pointer1++;
					} else if (curId < points[pointer1].id) {
						pointer2++;
						if (pointer2 < n2) {
							curId = fsReader.getLong();// mbb.getLong();
							curKey = fsReader.getFloat();// mbb.getFloat();
						}
					} else {
						bb.putLong(curId);
						bb.putFloat(curKey);
						counter++;

						if (counter >= defaultBlockSize) {
							bb.position(0);
							// fcout.write(bb);
							fsWritter.write(bb);

							counter = 0;
							bb.clear();
						}

						pointer1++;
						pointer2++;
						if (pointer2 < n2) {
							curId = fsReader.getLong();// mbb.getLong();
							curKey = fsReader.getFloat();// mbb.getFloat();
						}
					}
				}

				if (counter > 0) {
					bb.flip();
					// fcout.write(bb);
					fsWritter.write(bb);
				}

				// fcout.close();
				fsWritter.unmapOutputFile();
				fsReader.unmapInputFile();
				clean(bb);
			} catch (FileNotFoundException ex) {
				SystemLogger.error(ex);
			} catch (IOException ex) {
				SystemLogger.error(ex);
			}

			return n1;
		} else {
			try {
				FileSystemManager fsReader1 = FileSystemSelector
						.getFileSystem();
				fsReader1.mapInputFile(fn1, 0, n1 * itemSize);
				n1 = Math.min(n1, fsReader1.size() / itemSize);

				FileSystemManager fsReader2 = FileSystemSelector
						.getFileSystem();
				fsReader2.mapInputFile(fn2, 0, n2 * itemSize);
				n2 = Math.min(n2, fsReader2.size() / itemSize);

				FileSystemManager fsWriter = FileSystemSelector.getFileSystem();
				fsWriter.mapOutputFile(ofn);

				// FileChannel fc1 = new FileInputStream(fn1).getChannel();
				// n1 = Math.min(n12, fc1.size() / itemSize);
				// FileChannel fc2 = new FileInputStream(fn2).getChannel();
				// n2 = Math.min(n2, fc2.size() / itemSize);
				// MappedByteBuffer mbb1 =
				// fc1.map(FileChannel.MapMode.READ_ONLY,
				// 0, n1 * itemSize);
				// MappedByteBuffer mbb2 =
				// fc2.map(FileChannel.MapMode.READ_ONLY,
				// 0, n2 * itemSize);
				// FileChannel fcout = new FileOutputStream(fn1 + "-join")
				// .getChannel();

				ByteBuffer bb = ByteBuffer
						.allocateDirect((int) (defaultBlockSize * itemSize));

				long p1 = 0, p2 = 0;
				long id1 = fsReader1.getLong(), // mbb1.getLong(),
				id2 = fsReader2.getLong(); // mbb2.getLong();
				float k1 = fsReader1.getFloat(), // mbb1.getFloat(),
				k2 = fsReader2.getFloat();// mbb2.getFloat();
				int counter = 0;

				while (p1 < n1 && p2 < n2) {
					if (id1 < id2) {
						p1++;
						if (p1 < n1) {
							id1 = fsReader1.getLong();// mbb1.getLong();
							k1 = fsReader1.getFloat();// mbb1.getFloat();
						}
					} else if (id1 > id2) {
						p2++;
						if (p2 < n2) {
							id2 = fsReader2.getLong();// mbb2.getLong();
							k2 = fsReader2.getFloat();// mbb2.getFloat();
						}
					} else {
						bb.putLong(id2);
						bb.putFloat(k2);
						counter++;

						if (counter >= defaultBlockSize) {
							bb.position(0);
							// fcout.write(bb);
							fsWriter.write(bb);

							counter = 0;
							bb.clear();
						}

						p1++;
						if (p1 < n1) {
							id1 = fsReader1.getLong();// mbb1.getLong();
							k1 = fsReader1.getFloat();// mbb1.getFloat();
						}

						p2++;
						if (p2 < n2) {
							id2 = fsReader2.getLong();// mbb2.getLong();
							k2 = fsReader2.getFloat();// mbb2.getFloat();
						}
					}
				}

				if (counter > 0) {
					bb.flip();
					// fcout.write(bb);
					fsWriter.write(bb);
				}

				// used for remove warning
				k2 = k1;
				// fcout.close();
				// fc1.close();
				// unmap(fc1, mbb1);

				// fc2.close();
				// unmap(fc2, mbb2);

				fsReader1.unmapInputFile();
				fsReader2.unmapInputFile();
				fsWriter.unmapOutputFile();

				clean(bb);
			} catch (FileNotFoundException ex) {
				SystemLogger.error(ex);
			} catch (IOException ex) {
				SystemLogger.error(ex);
			} catch (Exception ex) {
				SystemLogger.error(ex);
			}

			return n1;
		}
	}

	public void GenerateData() {
		try {
			DataOutputStream dos = new DataOutputStream(new FileOutputStream(
					"c:\\tmp\\d2"));
			ArrayList<SortNode> sns = new ArrayList<>();
			for (int i = 0; i < 10000000; i++) {
				SortNode sn = new SortNode();
				sn.id = i;
				sn.key = 3 * i;

				sns.add(sn);
			}

			Collections.shuffle(sns);

			for (SortNode sn : sns) {
				dos.writeLong(sn.id);
				dos.writeFloat(sn.key);
			}

			dos.close();
		} catch (Exception e) {
		}

	}

	public void PrintDatafile(String filename) {
		try {
			DataInputStream dis = new DataInputStream(new FileInputStream(
					filename));
			while (true) {
				try {
					System.out.printf("%8d\t%8.2f\n", dis.readLong(),
							dis.readFloat());
				} catch (EOFException e) {
					dis.close();
					break;
				}
			}
		} catch (Exception e) {
		}
	}
}
