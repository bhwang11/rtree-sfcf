/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spatialindex.rtree;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;

/**
 * 
 * @author PowerWBH
 */
public class RTree {

	int dimension;
	int rootLevel;
	boolean bCreated;
	String treeName;
	Box mbr;
	ArrayList<Exception> errors;

	RTreeNode root = null;
	DataBlockCache[][] dbQueryCaches = null;

	static final String INTEM_FOIDER_NAME = "centers";

	public RTree(int d) {
		dimension = d;
		rootLevel = 0;
		bCreated = false;
		errors = new ArrayList<>();
		mbr = new Box(dimension);
		mbr.setMinMax();
	}
	
	public void reset() {
		rootLevel = 0;
		bCreated = false;
		errors.clear();
		mbr.setMinMax();
	}

	public void construct(String filename, long numRecords) {
		String dir = FileSystemSelector.getFileSystem().
				getParentPath(filename + "-0");
		
		construct(filename, dir, rootLevel, numRecords);
		try {
			if (bCreated) {
				FileSystemManager fsCreator = FileSystemSelector
						.getFileSystem();
				
				ByteBuffer bb = ByteBuffer.allocateDirect(dimension * 8);
				
				fsCreator.mapOutputFile(filename + "-tree-level-" + rootLevel);				
				for (int i = 0; i < dimension; i++) {
					bb.putFloat(mbr.lowerBounds[i]);
					bb.putFloat(mbr.upperBounds[i]);
				}
				bb.flip();
				fsCreator.write(bb);
				fsCreator.unmapOutputFile();
				
				Sorter.clean(bb);

				// new File(filename + "-tree-level-" +
				// rootLevel).createNewFile();
				treeName = filename;
				SystemLogger.info("Tree area is " + mbr.toString());
				SystemLogger.info("Tree Created");
				System.in.read();
			}
		} catch (Exception e) {
			SystemLogger.error(e);
		}
	}

	public void construct(String filename) {
		try {
			FileSystemManager fsm = FileSystemSelector.getFileSystem();
			String dir = fsm.getParentPath(filename + "-0");
			String levelFilename = fsm.findFilenameContain(filename
					+ "-tree-level-", dir);

			if (levelFilename != null) {
				rootLevel = Integer.valueOf(levelFilename
						.substring((filename + "-tree-level-").length()));
				
				FileSystemManager fsReader = FileSystemSelector.getFileSystem();
				fsReader.mapInputFile(levelFilename, 0, dimension * 8);
				for (int i = 0; i < dimension; i++) {
					mbr.lowerBounds[i] = fsReader.getFloat();
					mbr.upperBounds[i] = fsReader.getFloat();
				}
				fsReader.unmapInputFile();				
				
				bCreated = true;
				treeName = filename;
				SystemLogger.info("Tree Created");
			} else {
				SystemLogger.info("R Tree construction failed!");
			}
		} catch (Exception e) {
			SystemLogger.error(e);
		}
	}

	private long PartitionOneFileInMemory(String idsetFilename,
			String centerFilename,
			DataBlockCache dbc,
			// FileChannel fcLevel, FileChannel fcOriginal,
			FileSystemManager fsLevelWriter,
			FileSystemManager fsOriginalWriter, int numOneDim, int startDim,
			long startCnt) throws FileNotFoundException, IOException, Exception {

		ByteBuffer bb = ByteBuffer.allocateDirect(4096 * 1024);
		ByteBuffer bbo = ByteBuffer.allocateDirect((1 << 20)
				* (8 + dimension * 8));
		int counter = 0;
		// partition the data
		SystemLogger.info("make rest partition begins");
		ArrayList<RTreeNode> rtns = new Sorter().makeRestPartition(
				idsetFilename, centerFilename, startDim, dimension,
				8 + dimension * 4, numOneDim);
		SystemLogger.info("make rest partition ends & compute MBR starting");

		// store as a uncluster index
		// RandomAccessFile raf = new RandomAccessFile(origFilename, "r");
		// int originalRecordSize = 8 + 8 * dimension;
		long cnt = startCnt;
		int pageCnt = 0;
		// compute new MBR for each partition
		for (RTreeNode rtn : rtns) {
			float[] lbs = new float[dimension];
			float[] ubs = new float[dimension];

			for (int i = 0; i < dimension; i++) {
				lbs[i] = Float.MAX_VALUE;
				ubs[i] = -Float.MAX_VALUE;
			}

			pageCnt = bb.position();
			bb.putLong(rtn.dp.children.length);
			for (int i = 0; i < rtn.dp.children.length; i++) {
				ByteBuffer localbb = dbc.getData(rtn.dp.children[i]);
				// raf.seek(rtn.dp.children[i] * originalRecordSize);
				// raf.readLong();
				// for (int j = 0; j < dimension; j++) {
				// lbs[j] = Math.min(lbs[j], raf.readFloat());
				// ubs[j] = Math.max(ubs[j], raf.readFloat());
				// }

				long id = localbb.getLong();
				if (id != rtn.dp.children[i]) {
					throw new Exception("id mismatched "
							+ String.format("%d,%d", id, rtn.dp.children[i]));
				}

				for (int j = 0; j < dimension; j++) {
					lbs[j] = Math.min(lbs[j], localbb.getFloat());
					ubs[j] = Math.max(ubs[j], localbb.getFloat());
				}

				bb.putLong(rtn.dp.children[i]);
			}
			// each node occupy on page
			pageCnt += 4096;
			bb.position(pageCnt);
			// if buffer is full
			if (pageCnt >= 4096 * 1024) {
				// write id set to level file
				bb.position(0);
				// fcLevel.write(bb);
				fsLevelWriter.write(bb);
				bb.clear();
				SystemLogger.info("flush one node buffer");
			}

			// write new level mbrs
			bbo.putLong(cnt);
			for (int i = 0; i < dimension; i++) {
				bbo.putFloat(lbs[i]);
				bbo.putFloat(ubs[i]);
			}
			counter++;
			if (counter >= (1 << 20)) {
				bbo.position(0);
				// fcOriginal.write(bbo);
				fsOriginalWriter.write(bbo);
				bbo.clear();
				counter = 0;
				SystemLogger.info("flush one meta buffer");
			}
			cnt++;
		}

		if (counter > 0) {
			bbo.flip();
			// fcOriginal.write(bbo);
			fsOriginalWriter.write(bbo);
		}

		if (pageCnt > 0) {
			bb.limit(pageCnt);
			bb.position(0);
			// fcLevel.write(bb);
			fsLevelWriter.write(bb);
		}

		Sorter.clean(bb);
		Sorter.clean(bbo);

		SystemLogger.info("MBR computation ends");
		SystemLogger
				.info(String.format("cache hit rate: %f", dbc.getHitRate()));
		return cnt;
	}

	private void construct(String filename, String dir, int level,
			long numOfRecords) {
		long numOfObjsPerNode = RTreeNode.MAX_NUM_CHILDREN;
		int numOneDim = (int) (Math.ceil((Double) Math.pow(
				(double) numOfRecords / numOfObjsPerNode, 1.0 / dimension)));

		if (numOfRecords < RTreeNode.MAX_NUM_CHILDREN) {
			rootLevel = level;
			bCreated = true;
			return;
		}

		try {
			int newLevel = level + 1;
			long newNumRecords = 0;
			// save centers of each dimension to the different file
			String origFilename = filename + "-" + level;
			// FileChannel fcOriginal = new FileOutputStream(filename + "-"
			// + newLevel).getChannel();
			// FileChannel fcLevel = new FileOutputStream(filename + "-lvl-"
			// + newLevel).getChannel();
			FileSystemManager fsOriginalWriter = FileSystemSelector
					.getFileSystem();
			fsOriginalWriter.mapOutputFile(filename + "-" + newLevel);

			FileSystemManager fsLevelWriter = FileSystemSelector
					.getFileSystem();
			fsLevelWriter.mapOutputFile(filename + "-lvl-" + newLevel);

			DataBlockCache dbc = new DataBlockCache(8 + 8 * dimension, 128);

			// extract dimension value to center-level folder to processing.
			extractCenters(origFilename, level);
			dbc.mapFile(origFilename);

			// if partition on disk is needed?
			// no needed
			if (numOfRecords < Sorter.defaultBlockSize) {
				String idsetFilename = String.format("%s/%s-%d/d-0", dir,
						INTEM_FOIDER_NAME, level);
				String centerFilename = String.format("%s/%s-%d/d-center", dir,
						INTEM_FOIDER_NAME, level);

				newNumRecords = PartitionOneFileInMemory(idsetFilename,
						centerFilename, dbc, fsLevelWriter, fsOriginalWriter,
						numOneDim, 0, 0);
			} // needed
			else {
				long remainNumRecords = numOfRecords;
				// bfs partition files to make all partition smaller than
				// defaultBlockSize
				ArrayList<String> partitionFilenames = new ArrayList<>();
				ArrayList<Integer> partitionLevels = new ArrayList<>();
				ArrayList<Long> partitionSize = new ArrayList<>();
				int depth = 0;
				// intialize
				partitionFilenames.add(String.format("%s/%s-%d/d-%d", dir,
						INTEM_FOIDER_NAME, level, 0));
				partitionLevels.add(0);
				partitionSize.add(remainNumRecords);

				while (partitionFilenames.size() > 0) {
					if (partitionSize.get(0) < Sorter.defaultBlockSize) {
						break;
					}

					Sorter s = new Sorter();
					String DimFilename = partitionFilenames.remove(0);
					depth = partitionLevels.remove(0);
					long sizeL = partitionSize.remove(0);

					// sort
					s.sort(DimFilename, 0, sizeL);
					if (!FileSystemSelector.getFileSystem().rename(
							DimFilename + "-sort", DimFilename + "-")) {
						SystemLogger.debug("rename failed " + DimFilename);
					}
					DimFilename = DimFilename + "-";
					// partition
					remainNumRecords = s.makePartition(DimFilename, numOneDim,
							sizeL);
					// remove original files
					FileSystemSelector.getFileSystem().delete(DimFilename);
					SystemLogger
							.info("***************************************");
					SystemLogger
							.info("***************************************");
					SystemLogger.info("check partitions");
					for (int i = 0; i < numOneDim; i++) {
						String f = DimFilename + "-p-" + i;
						if (FileSystemSelector.getFileSystem().exists(f)) {
							FileSystemManager fsMeta = FileSystemSelector.getFileSystem();
							fsMeta.mapInputFile(f, 0, 1 << 50L);
							long ss = fsMeta.size();
							fsMeta.unmapInputFile();
							SystemLogger.info(f + " exists. size:" + ss);
						}
					}

					SystemLogger
							.info("***************************************");
					SystemLogger
							.info("***************************************");
					SystemLogger.info("level " + depth
							+ " sort & partition finished");
					// join all partitions to next attribute
					for (int i = 0; i < numOneDim; i++) {
						String pfilename = DimFilename + "-p-" + i;

						// new File(pfilename).exists()
						if (FileSystemSelector.getFileSystem()
								.exists(pfilename)) {
							SystemLogger.info("start joining file: "
									+ pfilename);
							remainNumRecords = s.join(pfilename, String.format(
									"%s/%s-%d/d-%d", dir, INTEM_FOIDER_NAME,
									level, depth + 1), pfilename + "-",
									remainNumRecords, numOfRecords);

							FileSystemSelector.getFileSystem()
									.delete(pfilename);
							SystemLogger.info("end joining file: " + pfilename);
							// if (!FileSystemSelector.getFileSystem().rename(
							// pfilename + "-join", pfilename + "-")) {
							// SystemLogger
							// .debug("rename failed " + pfilename);
							// }

							partitionFilenames.add(pfilename + "-");
							partitionLevels.add(depth + 1);
							partitionSize.add(remainNumRecords);
						}
					}
				}

				SystemLogger.info("*****************************");
				SystemLogger.info("*****************************");
				SystemLogger.info("iteration " + depth + " finished");

				// check correctness
				for (int i = 0; i < partitionLevels.size(); i++) {
					if (partitionLevels.get(i) != depth + 1) {
						throw new Exception("incorrect partitions");
					}
				}

				SystemLogger.info("*****************************");
				SystemLogger.info("*****************************");
				SystemLogger.info("memory partition begins");

				long cnt = 0;
				for (int i = 0; i < partitionFilenames.size(); i++) {
					String idsetFilename = partitionFilenames.get(i);
					String centerFilename = String.format("%s/%s-%d/d-center",
							dir, INTEM_FOIDER_NAME, level);
					SystemLogger.info("partition file: " + idsetFilename
							+ " center file: " + centerFilename);
					cnt = PartitionOneFileInMemory(idsetFilename,
							centerFilename, dbc, fsLevelWriter,
							fsOriginalWriter, numOneDim,
							partitionLevels.get(i), cnt);

					FileSystemSelector.getFileSystem().delete(idsetFilename);
				}

				SystemLogger.info("*****************************");
				SystemLogger.info("*****************************");
				SystemLogger.info("memory partition ends");

				newNumRecords = cnt;
			}

			// new level index created
			// new MBRs data created
			// fcLevel.close();
			// fcOriginal.close();
			fsOriginalWriter.unmapOutputFile();
			fsLevelWriter.unmapOutputFile();

			dbc.unmapFile();

			FileSystemSelector.getFileSystem().delete(
					String.format("%s/%s-%d", dir, INTEM_FOIDER_NAME, level));

			construct(filename, dir, newLevel, newNumRecords);
		} catch (FileNotFoundException ex) {
			SystemLogger.error(ex);
			errors.add(ex);
		} catch (IOException ex) {
			SystemLogger.error(ex);
			errors.add(ex);
		} catch (Exception ex) {
			SystemLogger.error(ex);
			errors.add(ex);
		}
	}

	private String extractCenters(String filename, int level) {
		try {
			// FileChannel fcin = new FileInputStream(filename).getChannel();
			// MappedByteBuffer mbb = fcin.map(FileChannel.MapMode.READ_ONLY, 0,
			// fcin.size());
			// long size = fcin.size() / (8 + dimension * 8);
			FileSystemManager fsReader = FileSystemSelector.getFileSystem();
			fsReader.mapInputFile(filename, 0, 1L << 50L);

			long size = fsReader.size() / (8 + dimension * 8);
			// String dirtoryName = new File(filename).getParent() + "\\" +
			// INTEM_FOIDER_NAME + "-" + level;
			String dirtoryName = FileSystemSelector.getFileSystem()
					.getParentPath(filename)
					+ "/"
					+ INTEM_FOIDER_NAME
					+ "-"
					+ level;
			int bufferSize = 1 << 20;

			// FileChannel[] fcouts = new FileChannel[dimension];
			// FileChannel fcoutc = null;
			FileSystemManager[] fsWriter = new FileSystemManager[dimension];
			FileSystemManager fscWriter = FileSystemSelector.getFileSystem();
			ByteBuffer[] bbs = new ByteBuffer[dimension];
			ByteBuffer bbc = ByteBuffer.allocateDirect(bufferSize
					* (8 + dimension * 4));
			int counter = 0;
			// make directory
			if (!FileSystemSelector.getFileSystem().mkdir(dirtoryName)) {
				throw new Exception("create folder failed");
			}

			// initialization
			for (int i = 0; i < dimension; i++) {
				fsWriter[i] = FileSystemSelector.getFileSystem();
				fsWriter[i].mapOutputFile(dirtoryName + "/d-" + i);
				// fcouts[i] = new FileOutputStream(dirtoryName + "\\d-" +
				// i).getChannel();
				bbs[i] = ByteBuffer.allocateDirect(bufferSize * (8 + 4));
			}

			// fcoutc = new FileOutputStream(dirtoryName +
			// "\\d-center").getChannel();
			fscWriter.mapOutputFile(dirtoryName + "/d-center");

			for (long cnt = 0; cnt < size; cnt++) {
				fsReader.getLong(); // mbb.getLong();
				bbc.putLong(cnt);

				for (int i = 0; i < dimension; i++) {
					float lb = fsReader.getFloat();
					float ub = fsReader.getFloat();
					float center = lb + ub;
					// mbb.getFloat()
					// +
					// mbb.getFloat();
					center *= 0.5f;
					bbs[i].putLong(cnt);
					bbs[i].putFloat(center);
					bbc.putFloat(center);
					
					mbr.lowerBounds[i] = Math.min(lb, mbr.lowerBounds[i]);
					mbr.lowerBounds[i] = Math.min(ub, mbr.lowerBounds[i]);
					
					mbr.upperBounds[i] = Math.max(lb, mbr.upperBounds[i]);
					mbr.upperBounds[i] = Math.max(ub, mbr.upperBounds[i]);
				}
				counter++;

				if (counter >= bufferSize) {
					bbc.position(0);
					// fcoutc.write(bbc);
					fscWriter.write(bbc);
					bbc.clear();

					for (int i = 0; i < bbs.length; i++) {
						bbs[i].position(0);
						// fcouts[i].write(bbs[i]);
						fsWriter[i].write(bbs[i]);
						bbs[i].clear();
					}

					counter = 0;
				}
			}

			if (counter > 0) {
				bbc.flip();
				// fcoutc.write(bbc);
				fscWriter.write(bbc);

				for (int i = 0; i < bbs.length; i++) {
					bbs[i].flip();
					// fcouts[i].write(bbs[i]);
					fsWriter[i].write(bbs[i]);
				}
			}

			for (int i = 0; i < dimension; i++) {
				// fcouts[i].close();
				fsWriter[i].unmapOutputFile();
				Sorter.clean(bbs[i]);
			}

			// fcoutc.close();
			// fcin.close();
			// Sorter.unmap(fcin, mbb);
			fscWriter.unmapOutputFile();
			fsReader.unmapInputFile();
			Sorter.clean(bbc);

			return dirtoryName;
		} catch (FileNotFoundException fnfe) {
			SystemLogger.error(fnfe);
			errors.add(fnfe);
		} catch (IOException ioe) {
			SystemLogger.error(ioe);
			errors.add(ioe);
		} catch (Exception e) {
			SystemLogger.error(e);
			errors.add(e);
		}

		return null;
	}

	public long convertDataset(String filename) {
		try {
			DataOutputStream dos = new DataOutputStream(new FileOutputStream(
					filename + "-b"));
			BufferedReader br = new BufferedReader(new FileReader(filename));
			String line = null;
			long cnt = 0;

			while ((line = br.readLine()) != null) {
				String[] ss = line.split(" ");

				dos.writeLong(cnt);
				for (int i = 0; i < dimension; i++) {
					dos.writeFloat(Float.valueOf(ss[i]));
					dos.writeFloat(Float.valueOf(ss[i + dimension]));
				}

				cnt++;
			}

			dos.close();
			br.close();

			return cnt;
		} catch (FileNotFoundException ex) {
			SystemLogger.error(ex);
		} catch (IOException ex) {
			SystemLogger.error(ex);
		}

		return -1;
	}

	private void readIndex() {
		root = new RTreeNode();
		root.dimension = dimension;
		root.dp = root.new DiskPointer();
		root.mp = root.new MemoryPointer();
		try {
			// FileChannel fc1 = new FileInputStream(treeName + "-" + rootLevel)
			// .getChannel();
			// FileChannel fc2 = new FileInputStream(treeName + "-lvl-"
			// + rootLevel).getChannel();
			FileSystemManager fsReader1 = FileSystemSelector.getFileSystem();
			fsReader1.mapInputFile(treeName + "-" + rootLevel, 0, 1L << 50L);

			FileSystemManager fsReader2 = FileSystemSelector.getFileSystem();
			fsReader2
					.mapInputFile(treeName + "-lvl-" + rootLevel, 0, 1L << 50L);

			ByteBuffer bb1 = ByteBuffer.allocateDirect((int) fsReader1.size());
			ByteBuffer bb2 = ByteBuffer.allocateDirect((int) fsReader2.size());

			fsReader1.readBlock(bb1, 0);
			fsReader2.readBlock(bb2, 0);

			fsReader1.unmapInputFile();
			fsReader2.unmapInputFile();

			// fc1.read(bb1);
			// fc2.read(bb2);
			// fc1.close();
			// fc2.close();

			bb1.flip();
			bb2.flip();

			int size = bb1.limit() / (8 + dimension * 8);

			root.mp.children = new RTreeNode[size];
			root.dp.children = new long[size];
			root.lowerBounds = new float[dimension];
			root.upperBounds = new float[dimension];

			for (int i = 0; i < dimension; i++) {
				root.lowerBounds[i] = Float.MAX_VALUE;
				root.upperBounds[i] = -Float.MAX_VALUE;
			}
			// load level 0
			for (int i = 0; i < size; i++) {
				RTreeNode rtn = new RTreeNode();
				rtn.dimension = dimension;

				rtn.dp = rtn.new DiskPointer();
				rtn.mp = rtn.new MemoryPointer();

				rtn.lowerBounds = new float[dimension];
				rtn.upperBounds = new float[dimension];

				rtn.id = bb1.getLong();
				for (int j = 0; j < dimension; j++) {
					rtn.lowerBounds[j] = bb1.getFloat();
					rtn.upperBounds[j] = bb1.getFloat();

					root.lowerBounds[j] = Math.min(root.lowerBounds[j],
							rtn.lowerBounds[j]);
					root.upperBounds[j] = Math.max(root.upperBounds[j],
							rtn.upperBounds[j]);
				}

				int pos = bb2.position();
				rtn.dp.children = new long[(int) bb2.getLong()];
				rtn.mp.children = new RTreeNode[rtn.dp.children.length];
				for (int j = 0; j < rtn.dp.children.length; j++) {
					rtn.dp.children[j] = bb2.getLong();
				}

				bb2.position(pos + 4096);

				root.mp.children[i] = rtn;
				root.dp.children[i] = i;
			}
			Sorter.clean(bb1);
			Sorter.clean(bb2);
			// ***********************************************************
			// load level 1
			// fc1 = new FileInputStream(treeName + "-" + (rootLevel - 1))
			// .getChannel();
			// fc2 = new FileInputStream(treeName + "-lvl-" + (rootLevel - 1))
			// .getChannel();

			fsReader1.mapInputFile(treeName + "-" + (rootLevel - 1), 0,
					1L << 50L);

			fsReader2.mapInputFile(treeName + "-lvl-" + (rootLevel - 1), 0,
					1L << 50L);

			bb1 = ByteBuffer.allocateDirect((int) fsReader1.size());
			bb2 = ByteBuffer.allocateDirect((int) fsReader2.size());

			fsReader1.readBlock(bb1, 0);
			fsReader2.readBlock(bb2, 0);

			fsReader1.unmapInputFile();
			fsReader2.unmapInputFile();

			// fc1.read(bb1);
			// fc2.read(bb2);
			// fc1.close();
			// fc2.close();
			bb1.flip();
			bb2.flip();

			for (int i = 0; i < root.dp.children.length; i++) {
				RTreeNode rtn = root.mp.children[i];
				for (int j = 0; j < rtn.mp.children.length; j++) {
					RTreeNode rtnNew = new RTreeNode();
					rtnNew.dimension = dimension;

					rtnNew.dp = rtnNew.new DiskPointer();
					rtnNew.mp = rtnNew.new MemoryPointer();

					rtnNew.lowerBounds = new float[dimension];
					rtnNew.upperBounds = new float[dimension];

					int recordSize = 8 + 8 * dimension;
					// read meta data
					bb1.position(recordSize * (int) rtn.dp.children[j]);
					rtnNew.id = bb1.getLong();
					if (rtnNew.id != rtn.dp.children[j]) {
						throw new Exception("id Mismatched!!!!");
					}

					for (int d = 0; d < dimension; d++) {
						rtnNew.lowerBounds[d] = bb1.getFloat();
						rtnNew.upperBounds[d] = bb1.getFloat();
					}
					// read children ids
					bb2.position((int) rtn.dp.children[j] * 4096);
					rtnNew.dp.children = new long[(int) bb2.getLong()];
					rtnNew.mp.children = new RTreeNode[rtnNew.dp.children.length];
					for (int k = 0; k < rtnNew.dp.children.length; k++) {
						rtnNew.dp.children[k] = bb2.getLong();
					}

					rtn.mp.children[j] = rtnNew;
				}
			}
		} catch (FileNotFoundException ex) {
			SystemLogger.error(ex);
		} catch (IOException ex) {
			SystemLogger.error(ex);
		} catch (Exception ex) {
			SystemLogger.error(ex);
		}
	}

	private void query(SpatialObject so, ArrayList<Long> results,
			ArrayList<Box> resultBoxes, int level, long id) {
		if (level >= 0) {
			int recordSize = (8 + dimension * 8);
			try {
				// fc = new FileInputStream(treeName + "-" +
				// level).getChannel();
				// MappedByteBuffer mbb = fc.map(FileChannel.MapMode.READ_ONLY,
				// id
				// * recordSize, recordSize);
				if (dbQueryCaches[level][0] == null) {
					dbQueryCaches[level][0] = new DataBlockCache(recordSize,
							1 << 6);
					dbQueryCaches[level][0].mapFile(treeName + "-" + level);
				}

				ByteBuffer bb = dbQueryCaches[level][0].getData(id);

				if (bb.getLong() != id) {
					throw new Exception("id mismatch!!!");
				}

				Box mbr = new Box(dimension);
				for (int i = 0; i < dimension; i++) {
					mbr.lowerBounds[i] = bb.getFloat();
					mbr.upperBounds[i] = bb.getFloat();
				}

				// fc.close();
				// Sorter.unmap(fc, mbb);

				if (!mbr.intersect(so)) {
					return;
				}
				// SystemLogger.info(String.format("query id=%d; query level=%d; MBR=%s",
				// id, level, mbr.toString()));

				if (level == 0) {
					results.add(id);
					resultBoxes.add(mbr);
				} else {
					// fc = new FileInputStream(treeName + "-lvl-" + level)
					// .getChannel();
					// mbb = fc.map(FileChannel.MapMode.READ_ONLY, id * 4096,
					// 4096);
					if (dbQueryCaches[level][1] == null) {
						dbQueryCaches[level][1] = new DataBlockCache(4096,
								1 << 3);
						dbQueryCaches[level][1].mapFile(treeName + "-lvl-"
								+ level);
					}

					// long cnt = mbb.getLong();
					bb = dbQueryCaches[level][1].getData(id);
					long cnt = bb.getLong();
					for (int i = 0; i < cnt; i++) {
						query(so, results, resultBoxes, level - 1, bb.getLong());
					}

					// fc.close();
					// Sorter.unmap(fc, mbb);
				}

			} catch (FileNotFoundException ex) {
				SystemLogger.error(ex);
			} catch (IOException ex) {
				SystemLogger.error(ex);
			} catch (Exception ex) {
				SystemLogger.error(ex);
			}
		}
	}

	private void query(SpatialObject so, ArrayList<Long> results,
			ArrayList<Box> resultBoxes, int level, RTreeNode rtn) {
		if (level >= rootLevel) {
			Box mbr = new Box(dimension);
			mbr.set(rtn.lowerBounds, rtn.upperBounds);

			if (!mbr.intersect(so)) {
				return;
			}

			// SystemLogger.info(String.format(
			// "query id=%d; query level=%d; MBR=%s", rtn.id, level,
			// mbr.toString()));
			for (RTreeNode children : rtn.mp.children) {
				query(so, results, resultBoxes, level - 1, children);
			}
		} else if (level >= 0) {
			Box mbr = new Box(dimension);
			mbr.set(rtn.lowerBounds, rtn.upperBounds);

			if (!mbr.intersect(so)) {
				return;
			}

			// SystemLogger.info(String.format("query id=%d; query level=%d; MBR=%s",
			// rtn.id, level, mbr.toString()));
			for (long id : rtn.dp.children) {
				query(so, results, resultBoxes, level - 1, id);
			}
		}

	}

	public void clear() {
		root = null;
	}

	public QueryResult query(SpatialObject so) {
		ArrayList<Long> results = new ArrayList<>();
		ArrayList<Box> resultBoxes = new ArrayList<>();
		if (root == null) {
			readIndex();
			dbQueryCaches = new DataBlockCache[rootLevel + 1][2];
			for (int i = 0; i <= rootLevel; i++) {
				dbQueryCaches[i][0] = null;
				dbQueryCaches[i][1] = null;
			}

			SystemLogger.info("partial index loading");
		}

		query(so, results, resultBoxes, rootLevel + 1, root);

		// SystemLogger.info("*****number of query result: " +
		// resultBoxes.size() + "*****");
		// for(int i = 0; i < resultBoxes.size(); i++) {
		// SystemLogger.info(resultBoxes.get(i).toString());
		// }
		// if (resultBoxes.size() != 1) {
		// System.out.println("shit~!!!!!!!!!!!");
		// try {
		// System.in.read();
		// } catch (IOException ex) {
		//
		// }
		// }
		QueryResult qr = new QueryResult();
		qr.resultIDs = results;
		qr.resultBoxes = resultBoxes;

		return qr;
	}

	public ArrayList<Exception> getErrors() {
		return errors;
	}

}
