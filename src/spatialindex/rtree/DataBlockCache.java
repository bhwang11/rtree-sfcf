package spatialindex.rtree;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * 
 * @author PowerWBH
 */
public class DataBlockCache {

	int blockSize;
	int numBlocks;
	int itemSize;
	long hitTimes;
	long accessTimes;
	// FileChannel fc;
	FileSystemManager fsReader = FileSystemSelector.getFileSystem();

	class DataBlock {

		public ByteBuffer data;
		public long time;
	}

	HashMap<Long, DataBlock> blocks;
	TreeMap<Long, Long> timeIDMap;

	public DataBlockCache(int itemSize, int numBlocks) {
		blockSize = 512 * itemSize;
		this.numBlocks = numBlocks;
		this.itemSize = itemSize;
	}

	public void mapFile(String filename) throws FileNotFoundException,
			IOException {
		// fc = new FileInputStream(filename).getChannel();
		fsReader.mapInputFile(filename, 0, 1L << 50L);
		blocks = new HashMap<>();
		timeIDMap = new TreeMap<>();
		hitTimes = 0;
		accessTimes = 0;
	}

	public void unmapFile() throws IOException, Exception {
		// fc.close();
		fsReader.unmapInputFile();
		blocks.clear();
		timeIDMap.clear();
	}

	public ByteBuffer getData(long idx) throws IOException {
		ByteBuffer bb = getBlock(idx * itemSize / blockSize);
		byte[] data = new byte[itemSize];

		try {
			bb.position((int) (idx * itemSize % blockSize));
			bb.get(data);
		} catch (BufferUnderflowException ex) {
			SystemLogger.debug(String.format("idx = %d, %d, %d", idx, idx
					* itemSize % blockSize, bb.limit()));
			SystemLogger.error(ex);
		}
		
		if (bb.capacity() != blockSize) {
			SystemLogger.error(new Exception("block size shrinked????"));
		}

		return ByteBuffer.wrap(data);
	}

	private ByteBuffer getBlock(long idx) throws IOException {
		DataBlock db = null;
		accessTimes++;
		if ((db = blocks.get(idx)) != null) {
			timeIDMap.remove(db.time);

			db.time = System.nanoTime();
			timeIDMap.put(db.time, idx);
			hitTimes++;

			return db.data;
		} else {
			ByteBuffer bb;
			if (blocks.size() > numBlocks) {
				Map.Entry<Long, Long> e = timeIDMap.firstEntry();
				db = blocks.get(e.getValue());
				blocks.remove(e.getValue());
				timeIDMap.remove(e.getKey());

				db.data.clear();
				//db.data.limit(blockSize);
				fsReader.readBlock(db.data, idx * blockSize);
				// fc.position(idx * blockSize);
				// fc.read(db.data);
				db.data.flip();
			} else {
				bb = ByteBuffer.allocateDirect(blockSize);
				fsReader.readBlock(bb, idx * blockSize);
				// fc.position(idx * blockSize);
				// fc.read(bb);
				bb.flip();

				db = new DataBlock();
				db.data = bb;
			}

			db.time = System.nanoTime();
			timeIDMap.put(db.time, idx);
			blocks.put(idx, db);
			return db.data;

		}
	}

	public float getHitRate() {
		return (float) hitTimes / accessTimes;
	}
}
