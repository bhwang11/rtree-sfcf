package spatialindex.rtree;

import java.util.ArrayList;

public class QueryResult {
	public ArrayList<Long> resultIDs;
	public ArrayList<Box> resultBoxes;
	
	public QueryResult() {
		resultIDs = new ArrayList<>();
		resultBoxes = new ArrayList<>();
	}
}
