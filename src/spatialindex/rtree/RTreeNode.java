/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spatialindex.rtree;

/**
 *
 * @author PowerWBH
 */
public class RTreeNode {

	public float[] lowerBounds;
	public float[] upperBounds;
	public int dimension;
	public int type;
	public long id;

	public MemoryPointer mp;
	public DiskPointer dp;

	public static final int MAX_NUM_CHILDREN = 200;

	public class MemoryPointer {

		RTreeNode parent;
		RTreeNode[] children;
	}

	public class DiskPointer {

		long[] children;
		long parent;
	}
}
