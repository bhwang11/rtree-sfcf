/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spatialindex.rtree;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * 
 * @author PowerWBH
 */
public class SystemLogger {
	static BufferedWriter bw = null;

	public static void setWriter(BufferedWriter bw) {
		SystemLogger.bw = bw;
	}

	public static void error(Exception e) {
		if (bw != null) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			pw.println(e.getMessage());
			e.printStackTrace(pw);
			pw.println();

			try {
				bw.write(sw.toString());
			} catch (IOException ioe) {

			}
		}

		System.err.println(e.getMessage());
		e.printStackTrace(System.err);
		System.err.println();
	}

	public static void info(String s) {
		if (bw != null) {
			try {
				bw.write(String.format("\033[33;1m[I][%.3f] %s\033[0m\n",
						System.nanoTime() / 1000000.0, s));
			} catch (IOException e) {
			}
		}
		System.out.printf("\033[33;1m[I][%.3f] %s\033[0m\n",
				System.nanoTime() / 1000000.0, s);
	}

	public static void debug(String s) {
		if (bw != null) {
			try {
				bw.write(String.format("\033[34;1m[D][%.3f] %s\033[0m\n",
						System.nanoTime() / 1000000.0, s));
			} catch (IOException e) {
			}
		}
		System.out.printf("\033[34;1m[D][%.3f] %s\033[0m\n",
				System.nanoTime() / 1000000.0, s);
	}

	public static void warning(String s) {
		if (bw != null) {
			try {
				bw.write(String.format("\033[35;1m[W][%.3f] %s\033[0m\n",
						System.nanoTime() / 1000000.0, s));
			} catch (IOException e) {
			}
		}
		System.out.printf("\033[35;1m[W][%.3f] %s\033[0m\n",
				System.nanoTime() / 1000000.0, s);
	}
}
