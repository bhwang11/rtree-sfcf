/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spatialindex;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;

import org.apache.hadoop.conf.Configuration;

import spatialindex.rtree.Box;
import spatialindex.rtree.FileSystemManager;
import spatialindex.rtree.FileSystemSelector;
import spatialindex.rtree.HDFSManager;
import spatialindex.rtree.Point;
import spatialindex.rtree.QueryResult;
import spatialindex.rtree.RTree;
import spatialindex.rtree.RTreeMemory;
import spatialindex.rtree.SystemLogger;
import spatialindex.rtree.Sorter;

/**
 *
 * @author PowerWBH
 */
public class SpatialIndex {

	public static class Pair {

		public double x, y;

		public double[] toArray() {
			double[] a = {x, y};
			return a;
		}
	}

	/**
	 * @param args the command line arguments
	 */
//	public static void main(String[] args) {
//		TreeMap<Long, Pair> coordSorted = new TreeMap<>();
//		ArrayList<Pair> coordinates = new ArrayList<>();
//		SFCFSceneMapper sfcfsm = new SFCFSceneMapper(2);
//		double[] lb = {0.0, 0.0};
//		double[] ub = {512.0, 512.0};
//		int[] nc = {16, 16};
//		double mx = 512.0, step = 32.0;
//
//		sfcfsm.setParams(lb, ub, nc);
//
//		for (double y = step * 0.5; y < mx; y += step) {
//			for (double x = step * 0.5; x < mx; x += step) {
//				Pair p = new Pair();
//				p.x = x;
//				p.y = y;
//				coordinates.add(p);
//			}
//		}
//
//		for (Pair p : coordinates) {
//			try {
//				coordSorted.put(sfcfsm.getZOrderValue(p.toArray()), p);
//			} catch (Exception e) {
//				System.err.println(e.getMessage());
//			}
//		}
//
//		try {
//			BufferedWriter bw = new BufferedWriter(new FileWriter("c:\\tmp\\dat.dd"));
//			for (Map.Entry<Long, Pair> kvp : coordSorted.entrySet()) {
//				bw.write(String.format("%.2f\t%.2f\n", kvp.getValue().x, kvp.getValue().y));
//			}
//
//			bw.close();
//		} catch (IOException ioe) {
//			System.err.println(ioe.getMessage());
//		}
//
//	}
//		Sorter s1 = new Sorter();
//		s.GenerateData();
//		 s.sort("c:\\tmp\\dd\\d", 0, 1000);
//		s.PrintDatafile("c:\\tmp\\dd\\d-p-111");
//		s.makePartition("c:\\tmp\\dd\\d", 1000, 10000000);
//		s1.sort("c:\\tmp\\dd\\d1", 0, 10000000);
//		System.out.println("sort finished, partition starts");
//		long size = s1.makePartition("c:\\tmp\\dd\\d1-sort", 10, 10000000);
//		System.out.println(size);
//		s1.sort("c:\\tmp\\dd\\d2", 0, 10000000);
//		s1.join("c:\\tmp\\dd\\d1-sort-p-1", "c:\\tmp\\dd\\d2-sort", 1000001, 10000000);
//		s1.PrintDatafile("c:\\tmp\\dd\\d1-sort-p-1-join");
	
	public static void testRTree(String filename, String testFilename, int dim, int numRecords) {
		try {
			RTree rt = new RTree(dim);
			long fileSize = new File(filename + "-0").length();			
			long nr = fileSize / (8 * (dim + 1));
			rt.construct(filename, nr);
			DataInputStream dis = new DataInputStream(new FileInputStream(testFilename));
			int c = 0;
			while (true) {
				try {
					Point p = new Point(dim);
					for (int i = 0; i < dim; i++) {
						p.vals[i] = dis.readFloat();
					}
					
					QueryResult result = rt.query(p);
					if (result.resultIDs.size() == 0) {
						System.err.println("error test " + p.toString());
						
					} else {
						System.out.printf("totally %d result(s)\n", result.resultIDs.size());
						for (Box b : result.resultBoxes) {
							if (b.contain(p) == false) {
								System.err.println("error test " + p.toString() + " " + b.toString());
							}
						}
					}
					c++;
					
//					if (c % 1 == 0) {
//						System.out.println(c);
//					}
				} catch (EOFException e) {
					break;
				}
			}

			dis.close();
		} catch (Exception e) {

		}
	}
	
	public static void main(String[] args) {
//		RectGenerator.GenerateRandRects("c:/tmp/rect/rect-rand2d");
//		try {
//			FileSystemSelector.config(FileSystemSelector.HDFS_FILESYSTEM_TYPE);
//			HDFSManager.init(URI.create("s3://dbprj/"), new Configuration());
//			//HDFSManager.init(new Configuration());
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		testRTree("c:/tmp/rect/rect-grid", "c:/tmp/rect/rect-grid-p", 2, 1 << 22);
		
//		RTree rt = new RTree(4);
//		rt.construct("s3://dbprj/rect-grid4d", 1 << 20);
		//System.out.println(rt.convertDataset("c:\\tmp\\rect\\rect"));
//		rt.construct("c:\\tmp\\rect\\rect-grid4d", 1 << 20);
//		rt.construct("c:\\tmp\\rect\\rect-grid4d");
//

//		RectGenerator.GenerateRects("c:\\tmp\\rect\\rect-grid8d");
		//RectGenerator.GenerateRectsText("c:\\tmp\\rect\\rect-grid-2d");
		//RectGenerator.GenerateRects("rect-grid-2d");
		
//		RTreeMemory rtree= new RTreeMemory();
//		
//		//rtree.construct("C:\\tmp\\rect\\rect-grid-2d", 2);
//		
//		try {
//			//FileChannel fc = new FileOutputStream("c:\\tmp\\rect\\rtree").getChannel();
//			FileChannel fc = new FileInputStream("c:\\tmp\\rect\\rtree").getChannel();
//			ByteBuffer bb = ByteBuffer.allocateDirect((int)fc.size());
//			fc.read(bb);
//			fc.close();
//			bb.flip();
//			rtree.readFrom(bb);
//		}catch(Exception e) {
//			
//		}
//		
//		
//		
//		ArrayList<String> records = new ArrayList<>();
//		try {
//			BufferedReader br = new BufferedReader(new FileReader("C:\\tmp\\rect\\rect-grid-2d-p"));
//			String line = null;
//			while ((line = br.readLine()) != null) {
//				records.add(line);
//			}
//			
//			br.close();
//		} catch (FileNotFoundException ex) {
//			SystemLogger.error(ex);
//		} catch (IOException ex) {
//			SystemLogger.error(ex);
//		}
//		
//		int cnt = 0;
//		for (String q : records) {
//			String[] fields = q.split(",");
//			Point p = new Point(2);
//			p.vals[0] = Float.valueOf(fields[0]);
//			p.vals[1] = Float.valueOf(fields[1]);
//			rtree.query(p);
//			cnt++;
//			
//			if (cnt % 1000 == 0) {
//				SystemLogger.info("[" + cnt + "]");
//			}
//		}
	}

}
