/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spatialindex;

import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import spatialindex.rtree.*;

/**
 * 
 * @author PowerWBH
 */
public class RectGenerator {

	public static void GenerateRects(String filename) {
		int numOneDim = 1 << 11;
		float len = 1 << 12;

		try {
			DataOutputStream dos = new DataOutputStream(new FileOutputStream(
					filename));
			DataOutputStream dosp = new DataOutputStream(new FileOutputStream(
					filename + "-p"));

			float cellLen = len / numOneDim;
			float rectLen = cellLen * 0.9f;
			long cnt = 0;
			for (int i1 = 0; i1 < numOneDim; i1++) {
				for (int i2 = 0; i2 < numOneDim; i2++) {
					// for (int i3 = 0; i3 < numOneDim; i3++) {
					// for (int i4 = 0; i4 < numOneDim; i4++) {
					dos.writeLong(cnt++);
					dos.writeFloat(i1 * cellLen - 0.5f * rectLen);
					dos.writeFloat(i1 * cellLen + 0.5f * rectLen);

					dos.writeFloat(i2 * cellLen - 0.5f * rectLen);
					dos.writeFloat(i2 * cellLen + 0.5f * rectLen);

					// dos.writeFloat(i3 * cellLen - 0.5f * rectLen);
					// dos.writeFloat(i3 * cellLen + 0.5f * rectLen);
					// dos.writeFloat(i4 * cellLen - 0.5f * rectLen);
					// dos.writeFloat(i4 * cellLen + 0.5f * rectLen);
					// dos.writeFloat(i5 * cellLen - 0.5f * rectLen);
					// dos.writeFloat(i5 * cellLen + 0.5f * rectLen);
					//
					// dos.writeFloat(i6 * cellLen - 0.5f * rectLen);
					// dos.writeFloat(i6 * cellLen + 0.5f * rectLen);
					//
					// dos.writeFloat(i7 * cellLen - 0.5f * rectLen);
					// dos.writeFloat(i7 * cellLen + 0.5f * rectLen);
					//
					// dos.writeFloat(i8 * cellLen - 0.5f * rectLen);
					// dos.writeFloat(i8 * cellLen + 0.5f * rectLen);
					dosp.writeFloat(i1 * cellLen);
					dosp.writeFloat(i2 * cellLen);
					// dosp.writeFloat(i3 * cellLen);
					// dosp.writeFloat(i4 * cellLen);
					// dosp.writeFloat(i5 * cellLen);
					// dosp.writeFloat(i6 * cellLen);
					// dosp.writeFloat(i7 * cellLen);
					// dosp.writeFloat(i8 * cellLen);

					if (cnt % 1000 == 0) {
						System.out.println(cnt);
					}

					// }
					// }
				}
			}

			dos.close();
			dosp.close();
		} catch (Exception e) {
		}

	}

	public static void GenerateRandRects(String filename) {
		int numRect = 1 << 20;
		float boundary = 1 << 12;
		float len = 2;
		int dim = 2;
		ArrayList<Box> boxes = new ArrayList<>();
		ArrayList<Point> points = new ArrayList<>();
		
		for (int i = 0; i < numRect; i++) {
			float[] values = new float[dim];
			for (int j = 0; j < dim; j++) {
				values[j] = (float) Math.random() * (boundary - len);
			}
			
			Box b = new Box(dim);
			
			for (int j = 0; j < dim; j++) {
				b.lowerBounds[j] = values[j];
				b.upperBounds[j] = values[j] + (float) Math.random() * len;
			}
			
			boxes.add(b);
			points.add(b.center());
		}

		try {
			DataOutputStream dos = new DataOutputStream(new FileOutputStream(
					filename));
			DataOutputStream dosp = new DataOutputStream(new FileOutputStream(
					filename + "-p"));

			int cnt = 0;
			
			for (int i = 0; i < numRect; i++) {
				Box b = boxes.get(i);
				Point p = points.get(i);
				dos.writeLong(i);
				
				for (int d = 0; d <  dim; d++) {
					dos.writeFloat(b.lowerBounds[d]);
					dos.writeFloat(b.upperBounds[d]);
					
					dosp.writeFloat(p.vals[d]);
				}

				cnt++;
				if (cnt % 10000 == 0) {
					System.out.println(cnt);
				}
			}

			dos.close();
			dosp.close();
		} catch (Exception e) {
		}
	}

	public static void GenerateRectsText(String filename) {
		int numOneDim = 1 << 11;
		float len = 1 << 12;

		try {
			BufferedWriter dos = new BufferedWriter(new FileWriter(filename));
			BufferedWriter dosp = new BufferedWriter(new FileWriter(filename
					+ "-p"));

			float cellLen = len / numOneDim;
			float rectLen = cellLen * 0.9f;
			long cnt = 0;
			for (int i1 = 0; i1 < numOneDim; i1++) {
				for (int i2 = 0; i2 < numOneDim; i2++) {
					dos.write(String.format("%d,%f,%f,%f,%f\n", cnt, i1
							* cellLen - 0.5f * rectLen, i1 * cellLen + 0.5f
							* rectLen, i2 * cellLen - 0.5f * rectLen, i2
							* cellLen + 0.5f * rectLen));

					dosp.write(String.format("%f,%f\n", i1 * cellLen, i2
							* cellLen));
					cnt++;
					if (cnt % 1000 == 0) {
						System.out.println(cnt);
					}

				}
			}

			dos.close();
			dosp.close();
		} catch (Exception e) {
		}

	}
}
