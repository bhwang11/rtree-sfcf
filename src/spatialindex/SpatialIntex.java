/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spatialintex;

import spatialindex.sfcf.SFCFSceneMapper;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import spatialindex.rtree.Point;
import spatialindex.rtree.RTree;
import spatialindex.rtree.Sorter;

/**
 *
 * @author PowerWBH
 */
public class SpatialIntex {

	public static class Pair {

		public double x, y;

		public double[] toArray() {
			double[] a = {x, y};
			return a;
		}
	}

	/**
	 * @param args the command line arguments
	 */
//	public static void main(String[] args) {
//		TreeMap<Long, Pair> coordSorted = new TreeMap<>();
//		ArrayList<Pair> coordinates = new ArrayList<>();
//		SFCFSceneMapper sfcfsm = new SFCFSceneMapper(2);
//		double[] lb = {0.0, 0.0};
//		double[] ub = {512.0, 512.0};
//		int[] nc = {16, 16};
//		double mx = 512.0, step = 32.0;
//
//		sfcfsm.setParams(lb, ub, nc);
//
//		for (double y = step * 0.5; y < mx; y += step) {
//			for (double x = step * 0.5; x < mx; x += step) {
//				Pair p = new Pair();
//				p.x = x;
//				p.y = y;
//				coordinates.add(p);
//			}
//		}
//
//		for (Pair p : coordinates) {
//			try {
//				coordSorted.put(sfcfsm.getZOrderValue(p.toArray()), p);
//			} catch (Exception e) {
//				System.err.println(e.getMessage());
//			}
//		}
//
//		try {
//			BufferedWriter bw = new BufferedWriter(new FileWriter("c:\\tmp\\dat.dd"));
//			for (Map.Entry<Long, Pair> kvp : coordSorted.entrySet()) {
//				bw.write(String.format("%.2f\t%.2f\n", kvp.getValue().x, kvp.getValue().y));
//			}
//
//			bw.close();
//		} catch (IOException ioe) {
//			System.err.println(ioe.getMessage());
//		}
//
//	}
//		Sorter s1 = new Sorter();
//		s.GenerateData();
//		 s.sort("c:\\tmp\\dd\\d", 0, 1000);
//		s.PrintDatafile("c:\\tmp\\dd\\d-p-111");
//		s.makePartition("c:\\tmp\\dd\\d", 1000, 10000000);
//		s1.sort("c:\\tmp\\dd\\d1", 0, 10000000);
//		System.out.println("sort finished, partition starts");
//		long size = s1.makePartition("c:\\tmp\\dd\\d1-sort", 10, 10000000);
//		System.out.println(size);
//		s1.sort("c:\\tmp\\dd\\d2", 0, 10000000);
//		s1.join("c:\\tmp\\dd\\d1-sort-p-1", "c:\\tmp\\dd\\d2-sort", 1000001, 10000000);
//		s1.PrintDatafile("c:\\tmp\\dd\\d1-sort-p-1-join");
	public static void main(String[] args) {
		RTree rt = new RTree(2);
		//System.out.println(rt.convertDataset("c:\\tmp\\rect\\rect"));
		//rt.construct("c:\\tmp\\rect\\rect-grid", 1 << 22);
		rt.construct("c:\\tmp\\rect\\rect-grid");

		try {
			DataInputStream dis = new DataInputStream(new FileInputStream("c:\\tmp\\rect\\rect-grid-p"));
			int c = 0;
			while (true) {
				try {
					Point p = new Point(2);
					p.vals[0] = dis.readFloat();
					p.vals[1] = dis.readFloat();
					rt.query(p);
					c++;
					if (c % 1000 == 0) {
						System.out.println(c);
					}
				} catch (EOFException e) {
					break;
				}
			}

			dis.close();
		} catch (Exception e) {

		}
		//RectGenerator.GenerateRects("c:\\tmp\\rect\\rect-grid");
	}

}
